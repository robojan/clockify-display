#include <settings.h>
#include <spdlog/sinks/daily_file_sink.h>
#include <spdlog/sinks/dist_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>
#include <spdlog_formatters.h>

#include <QCoroQml>
#include <QCoroQmlTask>
#include <QDir>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlExtensionPlugin>
#include <QQmlFileSelector>
#include <QThread>
#include <QtWebEngineQuick>
#include <fstream>
#include <iostream>

#include "clockify/api.h"

#ifdef WIN32
#include <Windows.h>
#include <spdlog/sinks/msvc_sink.h>
#include <tchar.h>
#else
#include <openssl/ssl.h>
#endif

// Statically link to the plugins
Q_IMPORT_QML_PLUGIN(nl_robojan_controlsPlugin)

void qtMessageHandler(QtMsgType type, const QMessageLogContext &context,
                      const QString &msg) {
    spdlog::source_loc source(context.file, context.line, context.function);
    auto utf8Msg = msg.toStdString();
    auto logger = spdlog::get("qt");
    switch (type) {
        case QtDebugMsg:
            logger->log(source, spdlog::level::debug, utf8Msg);
            break;
        case QtInfoMsg:
            logger->log(source, spdlog::level::info, utf8Msg);
            break;
        default:
            [[fallthrough]];
        case QtWarningMsg:
            logger->log(source, spdlog::level::warn, utf8Msg);
            break;
        case QtCriticalMsg:
            logger->log(source, spdlog::level::err, utf8Msg);
            break;
        case QtFatalMsg:
            logger->log(source, spdlog::level::critical, utf8Msg);
            break;
    }
}

void initDebugger() {
    auto distributer_sink = std::make_shared<spdlog::sinks::dist_sink_mt>();

    auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    distributer_sink->add_sink(console_sink);

    auto daily_file_sink =
        std::make_shared<spdlog::sinks::daily_file_format_sink_mt>(
            "logs/rj_display.log", 0, 0);
    distributer_sink->add_sink(daily_file_sink);

#ifdef WIN32
    // Recreate the console
    AllocConsole();
    FILE *dummy;
    freopen_s(&dummy, "CONIN$", "r", stdin);
    freopen_s(&dummy, "CONOUT$", "w", stdout);
    freopen_s(&dummy, "CONOUT$", "w", stderr);
    std::cout.clear();
    std::cin.clear();
    std::clog.clear();
    std::cerr.clear();

    // std::wcout, std::wclog, std::wcerr, std::wcin
    HANDLE hConOut = CreateFile(_T("CONOUT$"), GENERIC_READ | GENERIC_WRITE,
                                FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                                OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    HANDLE hConIn = CreateFile(_T("CONIN$"), GENERIC_READ | GENERIC_WRITE,
                               FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                               OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    SetStdHandle(STD_OUTPUT_HANDLE, hConOut);
    SetStdHandle(STD_ERROR_HANDLE, hConOut);
    SetStdHandle(STD_INPUT_HANDLE, hConIn);
    std::wcout.clear();
    std::wclog.clear();
    std::wcerr.clear();
    std::wcin.clear();

    auto msvc_sink = std::make_shared<spdlog::sinks::msvc_sink_mt>();
    distributer_sink->add_sink(msvc_sink);
#endif

    auto logger = std::make_shared<spdlog::logger>("def", distributer_sink);

    spdlog::set_default_logger(logger);

    spdlog::set_level(spdlog::level::debug);

    // Create loggers
    spdlog::initialize_logger(logger->clone("qt"));

    qInstallMessageHandler(qtMessageHandler);
}

int main(int argc, char *argv[]) {
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QtWebEngineQuick::initialize();

    QGuiApplication app(argc, argv);

    QGuiApplication::setOrganizationName("Robbert-Jan");
    QGuiApplication::setOrganizationDomain("robojan.nl");
    QGuiApplication::setApplicationName(
        QFileInfo(QGuiApplication::applicationFilePath()).baseName());

    initDebugger();

    spdlog::info(
        "######################################################################"
        "####");
    spdlog::info("Starting RJ's desk display");

    QCoro::Qml::registerTypes();

    const QUrl url("qrc:/qt/qml/nl/robojan/display/Main.qml");
    QQmlApplicationEngine engine;
    QObject::connect(
        &engine, &QQmlApplicationEngine::objectCreated, &app,
        [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl) QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);
    engine.load(url);
    new QQmlFileSelector(&engine, &engine);

    auto profile = QWebEngineProfile::defaultProfile();
    spdlog::info("profile download path: {}", profile->downloadPath());
    spdlog::info("profile cache path: {}", profile->cachePath());
    spdlog::info("profile user agent: {}", profile->httpUserAgent());
    spdlog::info("profile persistent storage path: {}",
                 profile->persistentStoragePath());
    spdlog::info("profile persistent cookies policy: {}",
                 profile->persistentCookiesPolicy());
    auto cookiestore = profile->cookieStore();
    QObject::connect(cookiestore, &QWebEngineCookieStore::cookieAdded,
                     [](const QNetworkCookie &cookie) {
                         spdlog::info("Cookie added: {} {}={}", cookie.domain(),
                                      QString::fromUtf8(cookie.name()),
                                      QString::fromUtf8(cookie.value()));
                     });
    QObject::connect(cookiestore, &QWebEngineCookieStore::cookieRemoved,
                     [](const QNetworkCookie &cookie) {
                         spdlog::info("Cookie removed: {} {}={}",
                                      cookie.domain(),
                                      QString::fromUtf8(cookie.name()),
                                      QString::fromUtf8(cookie.value()));
                     });

    return QGuiApplication::exec();
}
