#pragma once

#include <clockify/api.h>

#include <QColor>
#include <QCoroQmlTask>
#include <QDateTime>
#include <QQmlEngine>
#include <optional>

#include "models/gadgetlistmodel.h"
#include "models/task_list_model.h"
#include "settings.h"

class Clockify : public QObject {
    Q_OBJECT
    QML_NAMED_ELEMENT(Clockify)
    QML_SINGLETON

    Q_PROPERTY(TaskListModel *templateTasks MEMBER _templateTasks NOTIFY
                   templateTasksChanged)
    Q_PROPERTY(bool taskRunning READ isTaskRunning NOTIFY taskRunningChanged)
    Q_PROPERTY(QDateTime activeTaskStartTime READ activeTaskStartTime NOTIFY
                   activeTaskStartTimeChanged)
    Q_PROPERTY(QString activeTaskProject READ activeTaskProject NOTIFY
                   activeTaskProjectChanged)
    Q_PROPERTY(QColor activeTaskProjectColor READ activeTaskProjectColor NOTIFY
                   activeTaskProjectColorChanged)
    Q_PROPERTY(QString activeTaskDescription READ activeTaskDescription NOTIFY
                   activeTaskDescriptionChanged)
    Q_PROPERTY(const GadgetListModel<clockify::TimeEntry> *todaysEntries READ
                   getTodaysEntries CONSTANT)

public:
    Clockify();

    Q_INVOKABLE QCoro::QmlTask taskActivated(const QString &description,
                                             const QString &projectId = "",
                                             const QString &taskId = "");
    Q_INVOKABLE QCoro::QmlTask stopActiveTask();

    Q_INVOKABLE QVariant findProject(const QString &pattern) const;
    Q_INVOKABLE QCoro::QmlTask findOrCreateTask(clockify::Project project,
                                                const QString &pattern) const;

    bool isTaskRunning() const { return _activeEntry.running; }
    QDateTime activeTaskStartTime() const {
        return isTaskRunning() ? _activeEntry.startTime : QDateTime();
    };
    QString activeTaskProject() const {
        return isTaskRunning() ? _activeEntry.project : QStringLiteral("");
    }
    QString activeTaskDescription() const {
        return isTaskRunning() ? _activeEntry.description : QStringLiteral("");
    };
    QColor activeTaskProjectColor() const {
        return isTaskRunning() ? _activeEntry.projectColor
                               : QColorConstants::Black;
    }
    const GadgetListModel<clockify::TimeEntry> *getTodaysEntries() const {
        return _todaysEntries;
    }

signals:
    void templateTasksChanged();
    void taskRunningChanged();
    void activeTaskStartTimeChanged();
    void activeTaskProjectChanged();
    void activeTaskDescriptionChanged();
    void activeTaskProjectColorChanged();

public slots:
    QCoro::Task<> updateTodaysEntries();

private:
    clockify::Connection _conn;
    std::optional<clockify::Workspace> _activeWorkspace;
    // Information about the currently running task
    struct {
        bool running = false;
        QString id;
        QDateTime startTime;
        QString project;
        QColor projectColor;
        QString description;
    } _activeEntry;
    TaskListModel *_templateTasks;
    GadgetListModel<clockify::TimeEntry> *_todaysEntries{
        new GadgetListModel<clockify::TimeEntry>(this)};
    std::vector<clockify::Project> _projects;

    void loadTemplateTasks();
    QCoro::Task<> updateActiveTask();
};
