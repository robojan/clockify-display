#include "task_list_model.h"

QVariant TaskListModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return {};
    if (index.row() >= _items.size()) return {};

    auto item = _items.at(index.row());

    switch (role) {
        case ItemRole:
            return QVariant::fromValue(item);
        case NameRole:
        case Qt::DisplayRole:
            return item->name();
        case DescriptionRole:
        case Qt::ToolTipRole:
            return item->description();
        case ProjectRole:
            return item->project();
        case ProjectIdRole:
            return item->projectId();
        case TaskRole:
            return item->task();
        case TaskIdRole:
            return item->taskId();
        default:
            return {};
    }
}

QVariant TaskListModel::headerData(int section, Qt::Orientation orientation,
                                   int role) const {
    if (role != Qt::DisplayRole) return {};

    return (orientation == Qt::Horizontal ? QStringLiteral("Column %1")
                                          : QStringLiteral("Row %1"))
        .arg(section);
}

QHash<int, QByteArray> TaskListModel::roleNames() const {
    auto roles = QAbstractItemModel::roleNames();
    roles.insert(ItemRole, "item");
    roles.insert(NameRole, "name");
    roles.insert(DescriptionRole, "description");
    roles.insert(ProjectRole, "project");
    roles.insert(ProjectIdRole, "projectId");
    roles.insert(TaskRole, "task");
    roles.insert(TaskIdRole, "taskId");
    return roles;
}

void TaskListModel::swapTasks(QList<TaskListItem *> &items) {
    beginResetModel();
    std::swap(_items, items);
    endResetModel();
}
