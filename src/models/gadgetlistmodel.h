#pragma once

#include <QAbstractListModel>
#include <QObject>
#include <concepts>

class GadgetListModelBase : public QAbstractListModel {
    Q_OBJECT

    Q_PROPERTY(qsizetype size READ size NOTIFY sizeChanged)

public:
    explicit GadgetListModelBase(QObject *parent = nullptr)
        : QAbstractListModel(parent) {}

    QHash<int, QByteArray> roleNames() const override = 0;
    virtual qsizetype size() const = 0;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override {
        return static_cast<int>(size());
    }
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override = 0;

    Q_INVOKABLE virtual QVariant get(qsizetype idx) const = 0;

    Q_INVOKABLE virtual void clear() = 0;

signals:
    void sizeChanged();
};

template <typename T>
concept GadgetObject =
    std::same_as<const QMetaObject, decltype(T::staticMetaObject)> &&
    !std::derived_from<T, QObject>;

template <GadgetObject T>
class GadgetListModel : public GadgetListModelBase {
    static constexpr int kObjectRole = Qt::UserRole;
    static constexpr int kFirstPropertyRole = kObjectRole + 1;

public:
    explicit GadgetListModel(QObject *parent = nullptr);

    qsizetype size() const override { return _data.size(); }
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;
    QVariant get(qsizetype idx) const override {
        if (idx < 0 || idx >= size()) {
            qWarning() << "GadgetListModel index out of range";
            return {};
        }
        return QVariant::fromValue(_data[idx]);
    }

    void clear() override;

    template <typename IIter>
    void replace(const IIter &begin, const IIter &end);

    QHash<int, QByteArray> roleNames() const override;

private:
    std::vector<T> _data;
    QList<int> _roles{kObjectRole};

    qsizetype numProperties() const {
        return T::staticMetaObject.propertyCount();
    }
};

// Implementation

template <GadgetObject T>
GadgetListModel<T>::GadgetListModel(QObject *parent)
    : GadgetListModelBase(parent) {
    for (int i = 0; i < numProperties(); i++) {
        _roles.push_back(kFirstPropertyRole + i);
    }
}

template <GadgetObject T>
QVariant GadgetListModel<T>::data(const QModelIndex &index, int role) const {
    if (!index.isValid() || index.column() != 0 || index.row() < 0 ||
        index.row() >= size()) {
        qWarning() << "Accessed the gadgetListModel out of range";
        return QVariant{};
    }
    const int lastPropertyRole = kFirstPropertyRole + numProperties() - 1;
    int row = index.row();
    if (role == kObjectRole) {
        return get(row);
    } else if (role >= kFirstPropertyRole && role <= lastPropertyRole) {
        auto property = T::staticMetaObject.property(role - kFirstPropertyRole);
        return property.readOnGadget(&_data[row]);
    } else {
        return QVariant{};
    }
}

template <GadgetObject T>
QHash<int, QByteArray> GadgetListModel<T>::roleNames() const {
    QHash<int, QByteArray> result;

    // Insert the static roles
    result.insert(kObjectRole, "qtObject");

    // Insert the roles for the properties
    const int propertyCount = T::staticMetaObject.propertyCount();
    for (int i = 0; i < propertyCount; i++) {
        auto propertyName = T::staticMetaObject.property(i).name();
        result.insert(kFirstPropertyRole + i, propertyName);
    }

    return result;
}

template <GadgetObject T>
void GadgetListModel<T>::clear() {
    beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
    _data.clear();
    endRemoveRows();
}

template <GadgetObject T>
template <typename IIter>
void GadgetListModel<T>::replace(const IIter &begin, const IIter &end) {
    auto newSize = std::distance(begin, end);
    auto oldSize = _data.size();
    if (newSize < size()) {
        beginRemoveRows(QModelIndex(), newSize, rowCount() - 1);
        _data.clear();
        _data.insert(_data.begin(), begin, end);
        endRemoveRows();
        emit dataChanged(index(0), index(rowCount() - 1), _roles);
    } else if (newSize > size()) {
        beginInsertRows(QModelIndex(), rowCount(), newSize - 1);
        _data.clear();
        _data.insert(_data.begin(), begin, end);
        endInsertRows();
        emit dataChanged(index(0), index(oldSize - 1), _roles);
    } else if (newSize == 0) {
        // The old size is identical to the new size, but that was empty. Do not
        // do anything.
    } else {
        _data.clear();
        _data.insert(_data.begin(), begin, end);
        emit dataChanged(index(0), index(rowCount() - 1), _roles);
    }
}
