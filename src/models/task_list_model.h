#pragma once

#include <QAbstractListModel>
#include <QList>

class TaskListItem : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString name MEMBER _name NOTIFY nameChanged)
    Q_PROPERTY(
        QString description MEMBER _description NOTIFY descriptionChanged)
    Q_PROPERTY(QString project MEMBER _project NOTIFY projectChanged)
    Q_PROPERTY(QString projectId MEMBER _projectId NOTIFY projectIdChanged)
    Q_PROPERTY(QString task MEMBER _task NOTIFY taskChanged)
    Q_PROPERTY(QString taskId MEMBER _taskId NOTIFY taskIdChanged)

public:
    TaskListItem(const QString &name, const QString &description,
                 const QString &project = "", const QString &projectId = "",
                 const QString &task = "", const QString &taskId = "")
        : _name(name),
          _description(description),
          _project(project),
          _projectId(projectId),
          _task(task),
          _taskId(taskId) {}

    auto &name() const { return _name; }
    auto &description() const { return _description; }
    auto &project() const { return _project; }
    auto &projectId() const { return _projectId; }
    auto &task() const { return _task; }
    auto &taskId() const { return _taskId; }

signals:
    void nameChanged();
    void descriptionChanged();
    void projectChanged();
    void projectIdChanged();
    void taskChanged();
    void taskIdChanged();

private:
    QString _name;
    QString _description;
    QString _project;
    QString _projectId;
    QString _task;
    QString _taskId;
};

class TaskListModel : public QAbstractListModel {
    Q_OBJECT

public:
    enum UserRoles {
        ItemRole = Qt::UserRole,
        NameRole,
        DescriptionRole,
        ProjectRole,
        ProjectIdRole,
        TaskRole,
        TaskIdRole,
    };

    explicit TaskListModel(QObject *parent = nullptr)
        : QAbstractListModel(parent) {}

    int rowCount(const QModelIndex & = QModelIndex()) const override {
        return static_cast<int>(_items.size());
    }

    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const override;

    void swapTasks(QList<TaskListItem *> &tasks);

private:
    QList<TaskListItem *> _items;
};
