#pragma once

#include <jira_connection.h>

#include <QCoroQmlTask>
#include <QObject>
#include <QQmlEngine>

class Jira : public QObject {
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON
public:
    Jira(QObject *parent = nullptr);

    Q_INVOKABLE QCoro::QmlTask getCurrentIssues();
    Q_INVOKABLE QCoro::QmlTask getProjectIssues(const QString &project);

private:
    std::unique_ptr<jira::Connection> _connection;
};
