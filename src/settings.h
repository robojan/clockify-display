#pragma once

#include <QQmlEngine>
#include <nlohmann/json.hpp>

class ClockifySettings : public QObject {
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("Settings are managed by the settings object.");

    Q_PROPERTY(QString apiKey READ apiKey WRITE setApiKey NOTIFY apiKeyChanged)
public:
    ClockifySettings &operator=(const nlohmann::json &j);

    nlohmann::json serialize() const;

    auto &apiKey() const { return _apiKey; }
    void setApiKey(const QString &apiKey);

signals:
    void apiKeyChanged();

private:
    QString _apiKey;
};

class AtlassianSettings : public QObject {
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("Settings are managed by the settings object.");

    Q_PROPERTY(
        QString username READ username WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(
        QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(QHash<QString, QString> customFields READ customFields WRITE
                   setCustomFields NOTIFY customFieldsChanged)
public:
    AtlassianSettings &operator=(const nlohmann::json &j);

    nlohmann::json serialize() const;

    auto &username() const { return _username; }
    void setUsername(const QString &username);
    auto &password() const { return _password; }
    void setPassword(const QString &password);
    auto &host() const { return _host; }
    void setHost(const QString &host);
    auto &customFields() const { return _customFields; }
    void setCustomFields(const QHash<QString, QString> &customFields);

signals:
    void usernameChanged();
    void passwordChanged();
    void hostChanged();
    void customFieldsChanged();

private:
    QString _username;
    QString _password;
    QString _host;
    QHash<QString, QString> _customFields;
};

class ClockifyProjectAndTask {
    Q_GADGET
    Q_PROPERTY(QString project MEMBER project)
    Q_PROPERTY(QString task MEMBER task)
public:
    QString project;
    QString task;
};

class EpicMappingSettings : public QObject {
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("Settings are managed by the settings object.");

public:
    EpicMappingSettings &operator=(const nlohmann::json &j);

    nlohmann::json serialize() const;

    Q_INVOKABLE QVariant getMapping(const QString &epic) const;

private:
    QHash<QString, ClockifyProjectAndTask> _mappings;
};

class GuiSettings : public QObject {
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("Settings are managed by the settings object.");

    Q_PROPERTY(bool fullscreen READ fullscreen WRITE setFullscreen NOTIFY
                   fullscreenChanged)
public:
    GuiSettings &operator=(const nlohmann::json &j);

    nlohmann::json serialize() const;

    auto fullscreen() const { return _fullscreen; }
    void setFullscreen(bool fullscreen);

signals:
    void fullscreenChanged();

private:
    bool _fullscreen = false;
};

class TaskItem : public QObject {
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("Only the settings should generate this object")

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY
                   descriptionChanged)
    Q_PROPERTY(
        QString project READ project WRITE setProject NOTIFY projectChanged)
    Q_PROPERTY(QString task READ task WRITE setTask NOTIFY taskChanged)

public:
    explicit TaskItem(const nlohmann::json &j);
    TaskItem &operator=(const nlohmann::json &j);

    nlohmann::json serialize() const;

    auto &name() const { return _name; }
    void setName(const QString &x);
    auto &description() const { return _description; }
    void setDescription(const QString &x);
    auto &project() const { return _project; }
    void setProject(const QString &x);
    auto &task() const { return _task; }
    void setTask(const QString &x);

signals:
    void nameChanged();
    void descriptionChanged();
    void projectChanged();
    void taskChanged();

private:
    QString _name;
    QString _description;
    QString _project;
    QString _task;
};

class Settings : public QObject {
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

    Q_PROPERTY(QString configFile READ getConfigFilePath CONSTANT);
    Q_PROPERTY(ClockifySettings *clockify READ clockify CONSTANT);
    Q_PROPERTY(AtlassianSettings *atlassian READ atlassian CONSTANT);
    Q_PROPERTY(GuiSettings *gui READ gui CONSTANT);
    Q_PROPERTY(EpicMappingSettings *epicMappings READ epicMappings CONSTANT)
    Q_PROPERTY(QList<TaskItem *> tasks READ tasks CONSTANT)

    static constexpr std::string_view kConfigFileName{"config.json"};
    static constexpr const char *kLoggingName{"settings"};

public:
    static Settings *create(QQmlEngine *qmlEngine, QJSEngine *jsEngine);
    static Settings &instance();

    Q_INVOKABLE void load();
    Q_INVOKABLE void save() const;

    static QString getConfigFilePath();

    ClockifySettings *clockify() { return &_clockify; }
    AtlassianSettings *atlassian() { return &_atlassian; }
    GuiSettings *gui() { return &_gui; }
    EpicMappingSettings *epicMappings() { return &_epicMappings; }
    auto &tasks() { return _tasks; }

private:
    Settings();

    ClockifySettings _clockify;
    AtlassianSettings _atlassian;
    GuiSettings _gui;
    EpicMappingSettings _epicMappings;
    QList<TaskItem *> _tasks;
};
