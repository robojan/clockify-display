
add_subdirectory(clockify)
add_subdirectory(models)

target_sources(clockify PRIVATE
    clockify.cpp
    clockify.h
    main.cpp
    settings.cpp
    settings.h
    jira.cpp
    jira.h
    )
