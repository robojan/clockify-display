#include "jira.h"

#include <settings.h>

Jira::Jira(QObject *parent) : QObject(nullptr) {
    auto settings = Settings::instance().atlassian();
    _connection = std::make_unique<jira::Connection>(*settings);
}

QCoro::QmlTask Jira::getCurrentIssues() {
    return _connection->search(
        u"assignee = currentUser() AND Sprint IN (openSprints()) AND status NOT IN (Closed) ORDER BY created DESC"_qs);
}

QCoro::QmlTask Jira::getProjectIssues(const QString &project) {
    return _connection->search(
        u"project = \"%1\" AND status NOT IN (Closed) ORDER BY created DESC"_qs
            .arg(project));
}
