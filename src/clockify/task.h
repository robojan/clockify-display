#pragma once

#include <QObject>
#include <QString>
#include <memory>
#include <nlohmann/json.hpp>

#include "interface.h"

namespace clockify {

class Task {
    Q_GADGET

    Q_PROPERTY(QString taskId READ taskId CONSTANT)
    Q_PROPERTY(QString projectId READ projectId CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
public:
    Task(std::shared_ptr<detail::Interface> itf, const nlohmann::json &j);
    Task() = default;
    Task(const Task &other) = default;
    Task(Task &&other) = default;
    ~Task() = default;

    Task &operator=(const Task &other) = default;
    Task &operator=(Task &&other) = default;

    auto &taskId() const { return _taskId; }
    auto &projectId() const { return _projectId; }
    auto &name() const { return _name; }

private:
    std::shared_ptr<detail::Interface> _itf;
    QString _taskId;
    QString _projectId;
    QString _name;
};

}  // namespace clockify
