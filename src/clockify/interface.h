#pragma once

#include <QCoroTask>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QStringView>
#include <QUrlQuery>
#include <nlohmann/json.hpp>
#include <string>
#include <string_view>

#include "userinfo.h"

namespace clockify {
namespace detail {

static constexpr QUtf8StringView kApiUri{"https://api.clockify.me/api/v1"};
static constexpr QUtf8StringView kReportsUri{
    "https://reports.api.clockify.me/v1"};
static constexpr QUtf8StringView kTimeOffUri{"https://pto.api.clockify.me/v1"};

class Interface : public QObject {
    Q_OBJECT
public:
    Interface(QStringView key) : _key(key.toUtf8()) {}
    QCoro::Task<bool> init();

    QCoro::Task<nlohmann::json> apiGet(
        QStringView path, const QUrlQuery &query = QUrlQuery{},
        QNetworkReply::NetworkError okCode = QNetworkReply::NoError) const;
    QCoro::Task<nlohmann::json> apiPost(
        QStringView path, const nlohmann::json &j,
        const QUrlQuery &query = QUrlQuery{},
        QNetworkReply::NetworkError okCode = QNetworkReply::NoError) const;
    QCoro::Task<nlohmann::json> apiPatch(
        QStringView path, const nlohmann::json &j,
        const QUrlQuery &query = QUrlQuery{},
        QNetworkReply::NetworkError okCode = QNetworkReply::NoError) const;
    const UserInfo &getUserInfo() const { return _user_info; }

protected:
    void addCommonHeaders(QNetworkRequest &request) const;

    nlohmann::json readResponse(
        QNetworkReply &reply, QNetworkReply::NetworkError okCode,
        QStringView uri = QStringLiteral("<unknown>")) const;

private:
    QByteArray _key;
    UserInfo _user_info;
    QNetworkAccessManager *_nam = new QNetworkAccessManager(this);

    QCoro::Task<> retrieveUserInfo();
};
}  // namespace detail
}  // namespace clockify
