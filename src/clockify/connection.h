#pragma once

#include <QCoroTask>
#include <QString>

#include "interface.h"
#include "userinfo.h"
#include "workspace.h"

namespace clockify {
class Connection {
public:
    Connection(QStringView key)
        : _itf(std::make_shared<detail::Interface>(key)) {}
    QCoro::Task<bool> init();

    const UserInfo &getUserInfo() const {
        assert(_itf);
        return _itf->getUserInfo();
    }
    QCoro::Task<std::vector<Workspace>> getWorkspaces() const;
    QCoro::Task<Workspace> activeWorkspace() const;

private:
    std::shared_ptr<detail::Interface> _itf;
};

}  // namespace clockify
