
#include "task.h"

#include "json_helpers.h"

namespace clockify {
Task::Task(std::shared_ptr<detail::Interface> itf, const nlohmann::json &j)
    : _itf(std::move(itf)),
      _taskId(j["id"].get<QString>()),
      _projectId(j["projectId"].get<QString>()),
      _name(j["name"].get<QString>()) {}
}  // namespace clockify
