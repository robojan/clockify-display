#pragma once

#include <QCoroTask>
#include <QObject>
#include <QString>
#include <memory>
#include <nlohmann/json.hpp>
#include <optional>

#include "interface.h"
#include "task.h"

namespace clockify {

class Project {
    Q_GADGET

    Q_PROPERTY(QString projectId READ id CONSTANT)
    Q_PROPERTY(QString workspaceId READ workspaceId CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString color READ color CONSTANT)
public:
    Project(std::shared_ptr<detail::Interface> itf, const nlohmann::json &j);
    Project() = default;
    Project(const Project &other) = default;
    Project(Project &&other) = default;
    ~Project() = default;

    Project &operator=(const Project &other) = default;
    Project &operator=(Project &&other) = default;

    auto &id() const { return _id; }
    auto &name() const { return _name; }
    auto &color() const { return _color; }
    auto &workspaceId() const { return _workspaceId; }

    std::optional<clockify::Task> findTask(const QString &pattern) const;
    QCoro::Task<clockify::Task> createTask(const QString &name);

private:
    std::shared_ptr<detail::Interface> _itf;
    QString _id;
    QString _name;
    QString _color;
    QString _workspaceId;
    std::vector<clockify::Task> _tasks;
};

}  // namespace clockify
