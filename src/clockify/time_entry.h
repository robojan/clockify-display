#pragma once

#include <QDateTime>
#include <QObject>
#include <QString>
#include <QVariant>
#include <nlohmann/json.hpp>

#include "project.h"

namespace clockify {

class TimeEntry {
    Q_GADGET
    Q_PROPERTY(QVariant description READ descriptionVariant CONSTANT)
    Q_PROPERTY(QDateTime start READ start CONSTANT)
    Q_PROPERTY(QVariant end READ endVariant CONSTANT)
public:
    explicit TimeEntry(std::shared_ptr<detail::Interface> itf,
                       const nlohmann::json &j);
    TimeEntry() = default;
    TimeEntry(const TimeEntry &other) = default;
    TimeEntry(TimeEntry &&other) = default;
    ~TimeEntry() = default;

    TimeEntry &operator=(const TimeEntry &other) = default;
    TimeEntry &operator=(TimeEntry &&other) = default;

    const QString &id() const { return _id; }
    auto &description() const { return _description; }
    QVariant descriptionVariant() const {
        return _description ? _description.value() : QVariant{};
    }
    auto &start() const { return _start; }
    auto &end() const { return _end; }
    QVariant endVariant() const { return _end ? _end.value() : QVariant{}; }
    auto &projectId() const { return _projectId; }
    auto &project() const { return _project; }

private:
    std::shared_ptr<detail::Interface> _itf;
    QString _id;
    std::optional<QString> _description;
    QDateTime _start;
    std::optional<QDateTime> _end;
    std::optional<QString> _projectId;
    std::optional<Project> _project;
};

}  // namespace clockify
