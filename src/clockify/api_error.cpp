#include "api_error.h"

namespace clockify {

ApiError::ApiError(int httpStatus, const nlohmann::json &j)
    : QException(), _httpStatus(httpStatus) {
    _message = j["message"].get<std::string>();
    _code = j["code"].get<int>();
}

}  // namespace clockify
