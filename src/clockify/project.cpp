
#include "project.h"

#include <QRegularExpression>
#include <algorithm>

#include "json_helpers.h"

namespace clockify {
Project::Project(std::shared_ptr<detail::Interface> itf,
                 const nlohmann::json &j)
    : _itf(std::move(itf)),
      _id(detail::getString(j, "id")),
      _name(detail::getString(j, "name")),
      _color(detail::getString(j, "color")),
      _workspaceId(j["workspaceId"].get<QString>()) {
    if (j.contains("tasks") && j["tasks"].is_array()) {
        for (auto &task : j["tasks"]) {
            _tasks.emplace_back(itf, task);
        }
    }
    // qDebug() << qPrintable(QString::fromStdString(j.dump(4)));
}

std::optional<clockify::Task> Project::findTask(const QString &pattern) const {
    QRegularExpression re{pattern};
    auto it = std::ranges::find_if(_tasks, [&re](const clockify::Task &task) {
        return re.match(task.name()).hasMatch();
    });
    return it != _tasks.end() ? std::make_optional(*it) : std::nullopt;
}

QCoro::Task<clockify::Task> Project::createTask(const QString &name) {
    nlohmann::json requestBody{{"name", name.toStdString()}};

    auto reply = co_await _itf->apiPost(
        u"/workspaces/%1/projects/%2/tasks"_qs.arg(_workspaceId).arg(_id),
        requestBody);

    co_return _tasks.emplace_back(_itf, reply);
}

}  // namespace clockify
