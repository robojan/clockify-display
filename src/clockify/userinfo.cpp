#include "userinfo.h"

#include "json_helpers.h"

namespace clockify {

UserInfo UserInfo::fromJson(const nlohmann::json &j) {
    return UserInfo{
        .id = detail::getString(j, "id"),
        .default_workspace = detail::getString(j, "defaultWorkspace"),
        .active_workspace = detail::getString(j, "activeWorkspace"),
        .email = detail::getString(j, "email"),
        .name = detail::getString(j, "name"),
    };
}

}  // namespace clockify
