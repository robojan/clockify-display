#pragma once

#include <QException>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <string>

namespace clockify {

class ApiError : public QException {
public:
    ApiError(int httpStatus, const nlohmann::json &j);

    char const *what() const noexcept override { return _message.c_str(); }

    auto &message() const { return _message; }
    auto code() const { return _code; }
    auto httpStatus() const { return _httpStatus; }

private:
    std::string _message;
    int _code;
    int _httpStatus;
};

class ConnectionError : public QException {
    [[nodiscard]] char const *what() const noexcept override {
        return "Failed to connect to the clockify service.";
    };
};

class ProtocolError : public QException {
    [[nodiscard]] char const *what() const noexcept override {
        return "Received an invalid response from the clockify service.";
    };
};

}  // namespace clockify
