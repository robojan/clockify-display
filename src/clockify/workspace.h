#pragma once

#include <QString>
#include <memory>
#include <nlohmann/json.hpp>

#include "project.h"
#include "time_entry.h"

namespace clockify {

class Workspace {
public:
    Workspace(std::shared_ptr<detail::Interface> itf, const nlohmann::json &j);
    Workspace() = default;
    Workspace(const Workspace &other) = default;
    Workspace(Workspace &&other) = default;
    ~Workspace() = default;

    Workspace &operator=(const Workspace &other) = default;
    Workspace &operator=(Workspace &&other) = default;

    const QString &id() const { return _id; }
    const QString &name() const { return _name; }

    QCoro::Task<std::vector<TimeEntry>> getTimeEntries() const;
    QCoro::Task<std::vector<TimeEntry>> getTimeEntries(QDateTime from,
                                                       QDateTime to) const;
    QCoro::Task<std::optional<TimeEntry>> getCurrentTimeEntry() const;
    QCoro::Task<TimeEntry> startEntry(QStringView description,
                                      QStringView projectId = u"",
                                      QStringView taskId = u"") const;
    QCoro::Task<TimeEntry> stopTimer() const;
    QCoro::Task<std::vector<Project>> getProjects() const;

private:
    std::shared_ptr<detail::Interface> _itf;
    QString _id;
    QString _name;
};

}  // namespace clockify
