#pragma once

#include <QString>
#include <nlohmann/json.hpp>

namespace clockify {

struct UserInfo {
    static UserInfo fromJson(const nlohmann::json &j);
    QString id;
    QString default_workspace;
    QString active_workspace;
    QString email;
    QString name;
};

}  // namespace clockify
