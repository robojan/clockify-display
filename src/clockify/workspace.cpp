#include "workspace.h"

#include "json_helpers.h"

namespace clockify {

Workspace::Workspace(std::shared_ptr<detail::Interface> itf,
                     const nlohmann::json &j)
    : _itf(std::move(itf)),
      _id(detail::getString(j, "id")),
      _name(detail::getString(j, "name")) {}

QCoro::Task<std::vector<TimeEntry>> Workspace::getTimeEntries() const {
    assert(_itf);

    auto uri = QStringLiteral("/workspaces/%1/user/%2/time-entries")
                   .arg(_id, _itf->getUserInfo().id);

    auto j = co_await _itf->apiGet(uri);

    std::vector<TimeEntry> entries;
    entries.reserve(j.size());

    for (auto jw : j) {
        entries.emplace_back(_itf, jw);
    }

    co_return entries;
}

QCoro::Task<std::vector<TimeEntry>> Workspace::getTimeEntries(
    QDateTime from, QDateTime to) const {
    assert(_itf);

    auto uri = QStringLiteral("/workspaces/%1/user/%2/time-entries")
                   .arg(_id, _itf->getUserInfo().id);

    QUrlQuery query{
        std::pair{u"start"_qs, from.toTimeSpec(Qt::UTC).toString(Qt::ISODate)},
        std::pair{u"end"_qs, to.toTimeSpec(Qt::UTC).toString(Qt::ISODate)},
    };

    auto j = co_await _itf->apiGet(uri, query);

    std::vector<TimeEntry> entries;
    entries.reserve(j.size());

    for (auto jw : j) {
        entries.emplace_back(_itf, jw);
    }

    co_return entries;
}

QCoro::Task<std::optional<TimeEntry>> Workspace::getCurrentTimeEntry() const {
    assert(_itf);

    auto uri = QStringLiteral(
                   "/workspaces/%1/user/%2/"
                   "time-entries")
                   .arg(_id, _itf->getUserInfo().id);
    QUrlQuery query;
    query.addQueryItem(u"in-progress"_qs, u"1"_qs);
    query.addQueryItem(u"hydrated"_qs, u"1"_qs);

    auto j = co_await _itf->apiGet(uri, query);

    if (j.size() == 0) {
        co_return std::nullopt;
    } else {
        co_return TimeEntry(_itf, j[0]);
    }
}

QCoro::Task<TimeEntry> Workspace::startEntry(QStringView description,
                                             QStringView projectId,
                                             QStringView taskId) const {
    assert(_itf);

    auto uri = QStringLiteral("/workspaces/%1/time-entries").arg(_id);

    nlohmann::json j;
    j["start"] = QDateTime::currentDateTimeUtc().toString(Qt::ISODate).toUtf8();
    if (description.size() > 0) {
        j["description"] = description.toUtf8();
    }
    if (projectId.size() > 0) {
        j["projectId"] = projectId.toUtf8();
    }
    if (taskId.size() > 0) {
        j["taskId"] = taskId.toUtf8();
    }

    auto response = co_await _itf->apiPost(uri, j);

    co_return TimeEntry(_itf, response);
}

QCoro::Task<TimeEntry> Workspace::stopTimer() const {
    assert(_itf);

    auto uri = QStringLiteral("/workspaces/%1/user/%2/time-entries")
                   .arg(_id, _itf->getUserInfo().id);

    nlohmann::json j;
    j["end"] = QDateTime::currentDateTimeUtc().toString(Qt::ISODate).toUtf8();

    auto response = co_await _itf->apiPatch(uri, j);

    co_return TimeEntry(_itf, response);
}

QCoro::Task<std::vector<Project>> Workspace::getProjects() const {
    assert(_itf);

    auto uri = QStringLiteral("/workspaces/%1/projects").arg(_id);

    QUrlQuery query;
    query.addQueryItem(QStringLiteral("hydrated"), QStringLiteral("true"));

    auto j = co_await _itf->apiGet(uri, query);

    std::vector<Project> entries;
    entries.reserve(j.size());

    for (auto jw : j) {
        entries.emplace_back(_itf, jw);
    }

    co_return entries;
}
}  // namespace clockify
