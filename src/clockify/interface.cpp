#include "interface.h"

#include <spdlog/spdlog.h>
#include <spdlog_formatters.h>

#include <QCoroNetwork>
#include <QCoroNetworkReply>
#include <QCoroTask>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <sstream>

#include "api_error.h"

namespace clockify::detail {

void Interface::addCommonHeaders(QNetworkRequest &request) const {
    request.setRawHeader("X-Api-Key", _key);
    request.setRawHeader("Accept", "application/json");
    request.setHeader(QNetworkRequest::UserAgentHeader, "rj-display/0.1");
}

nlohmann::json Interface::readResponse(QNetworkReply &response,
                                       QNetworkReply::NetworkError okCode,
                                       QStringView uri) const {
    auto response_body = response.readAll();

    spdlog::trace("Response:{}\n{}", uri, QString::fromUtf8(response_body));

    if (response_body.isEmpty()) {
        spdlog::error("Got no body from {}", uri);
        throw ConnectionError();
    }

    try {
        auto response_object = nlohmann::json::parse(response_body);

        if (response.error() != okCode) {
            spdlog::error("error retrieving {}[{}]: {}", uri, response.error(),
                          QString::fromUtf8(response_body));
            throw ApiError(response.error(), response_object);
        }
        return response_object;
    } catch (nlohmann::json::exception &e) {
        spdlog::error("Error parsing reply to {}:\n{}", uri, e.what());
        throw ProtocolError();
    }
}

QCoro::Task<nlohmann::json> detail::Interface::apiGet(
    QStringView path, const QUrlQuery &query,
    QNetworkReply::NetworkError okCode) const {
    auto uri = QUrl(detail::kApiUri.toString().append(path));
    uri.setQuery(query);

    spdlog::trace("GET {}", uri);

    QNetworkRequest request(uri);
    addCommonHeaders(request);

    QScopedPointer<QNetworkReply> reply{co_await _nam->get(request)};

    co_return readResponse(*reply, okCode, uri.toDisplayString());
}

QCoro::Task<nlohmann::json> Interface::apiPost(
    QStringView path, const nlohmann::json &j, const QUrlQuery &query,
    QNetworkReply::NetworkError okCode) const {
    auto uri = QUrl(detail::kApiUri.toString().append(path));
    uri.setQuery(query);

    spdlog::trace("POST {}", uri);

    QNetworkRequest request(uri);
    addCommonHeaders(request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    auto request_body = QByteArray::fromStdString(j.dump());
    QScopedPointer<QNetworkReply> reply{
        co_await _nam->post(request, request_body)};

    co_return readResponse(*reply, okCode, uri.toDisplayString());
}

QCoro::Task<nlohmann::json> Interface::apiPatch(
    QStringView path, const nlohmann::json &j, const QUrlQuery &query,
    QNetworkReply::NetworkError okCode) const {
    auto uri = QUrl(detail::kApiUri.toString().append(path));
    uri.setQuery(query);

    spdlog::trace("PATCH {}", uri);

    QNetworkRequest request(uri);
    addCommonHeaders(request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    auto request_body = QByteArray::fromStdString(j.dump());
    QScopedPointer<QNetworkReply> reply{
        co_await _nam->sendCustomRequest(request, "PATCH", request_body)};

    co_return readResponse(*reply, okCode, uri.toDisplayString());
}

QCoro::Task<bool> Interface::init() {
    co_await retrieveUserInfo();

    co_return true;
}

QCoro::Task<> Interface::retrieveUserInfo() {
    auto response = co_await apiGet(u"/user");

    _user_info = UserInfo::fromJson(response);
}

}  // namespace clockify::detail
