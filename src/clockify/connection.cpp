#include "connection.h"

#include "json_helpers.h"

namespace clockify {

QCoro::Task<std::vector<Workspace>> Connection::getWorkspaces() const {
    assert(_itf);
    auto j = co_await _itf->apiGet(u"/workspaces");

    std::vector<Workspace> workspaces;
    workspaces.reserve(j.size());

    for (auto jw : j) {
        workspaces.emplace_back(_itf, jw);
    }

    co_return workspaces;
}

QCoro::Task<Workspace> Connection::activeWorkspace() const {
    assert(_itf);
    auto j = co_await _itf->apiGet(u"/workspaces");

    for (auto jw : j) {
        if (detail::getString(jw, "id") ==
            _itf->getUserInfo().active_workspace) {
            co_return Workspace(_itf, jw);
        }
    }

    throw std::runtime_error("active workspace not found");
}

QCoro::Task<bool> Connection::init() { return _itf->init(); }

}  // namespace clockify
