#include "time_entry.h"

#include "json_helpers.h"

namespace clockify {

TimeEntry::TimeEntry(std::shared_ptr<detail::Interface> itf,
                     const nlohmann::json &j)
    : _itf(std::move(itf)), _id(detail::getString(j, "id")) {
    _description = detail::getOptionalString(j, "description");
    _projectId = detail::getOptionalString(j, "projectId");

    auto timeInterval = j["timeInterval"];

    _start = detail::getTimestamp(timeInterval, "start");
    _end = detail::getOptionalTimestamp(timeInterval, "end");

    // Some options are only available on a hydrated
    // object
    if (j.contains("project") && j["project"].is_object()) {
        _project = Project(_itf, j["project"]);
    }
}

}  // namespace clockify
