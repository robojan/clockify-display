#include "clockify.h"

#include <date/date.h>
#include <date/tz.h>
#include <spdlog/spdlog.h>

#include <QRegularExpression>
#include <algorithm>
#include <string_view>

#include "spdlog_formatters.h"

Clockify::Clockify()
    : _conn(Settings::instance().clockify()->apiKey()),
      _templateTasks(new TaskListModel(this)) {
    try {
        QCoro::waitFor(_conn.init());
        _activeWorkspace = QCoro::waitFor(_conn.activeWorkspace());
        _projects = QCoro::waitFor(_activeWorkspace->getProjects());

        loadTemplateTasks();
        QCoro::waitFor(updateActiveTask());
        QCoro::waitFor(updateTodaysEntries());
    } catch (clockify::ApiError &err) {
        spdlog::error("Error while initializing clockify API: {}",
                      err.message());
    }
}

QCoro::QmlTask Clockify::taskActivated(const QString &description,
                                       const QString &projectId,
                                       const QString &taskId) {
    return [](auto *self, QString description, QString projectId,
              QString taskId) -> QCoro::Task<> {
        spdlog::debug("Task {} activated", description);
        try {
            co_await self->_activeWorkspace->startEntry(description, projectId,
                                                        taskId);
        } catch (clockify::ApiError &err) {
            spdlog::error("Could not start time entry: {}", err.message());
        }

        co_await self->updateActiveTask();
    }(this, description, projectId, taskId);
}

QCoro::QmlTask Clockify::stopActiveTask() {
    return [](auto *self) -> QCoro::Task<> {
        spdlog::debug("Stopping active task");
        try {
            co_await self->_activeWorkspace->stopTimer();
        } catch (clockify::ApiError &err) {
            spdlog::error("Could not stop active task: {}", err.message());
        }
        co_await self->updateActiveTask();
    }(this);
}

QVariant Clockify::findProject(const QString &pattern) const {
    auto re = QRegularExpression(pattern);
    auto result = std::ranges::find_if(_projects.begin(), _projects.end(),
                                       [&re](const auto &p) {
                                           auto name = p.name();
                                           return re.match(name).hasMatch();
                                       });
    return result == _projects.end() ? QVariant{}
                                     : QVariant::fromValue(*result);
}

QCoro::QmlTask Clockify::findOrCreateTask(clockify::Project project,
                                          const QString &name) const {
    return [](clockify::Project project,
              QString name) -> QCoro::Task<clockify::Task> {
        if (auto t = project.findTask(name); t) {
            co_return *t;
        }
        qInfo() << "Creating task " << name << " under project "
                << project.name();
        co_return co_await project.createTask(name);
    }(project, name);
}

void Clockify::loadTemplateTasks() {
    spdlog::debug("Loading template tasks");
    auto configTasks = Settings::instance().tasks();

    QList<TaskListItem *> tasks;
    for (auto src : configTasks) {
        // Get the basic task name and description. Name is mandatory.
        auto &name = src->name();
        if (name.isEmpty()) {
            spdlog::warn("Template task is missing the mandatory field name");
            continue;
        }
        auto &description = src->description();
        auto &projectSrc = src->project();

        // Try to find a match for the project pattern. If not found skip this
        // current task.
        QString project, projectId;
        if (!projectSrc.isEmpty()) {
            auto projectObj = findProject(projectSrc);
            if (!projectObj.isValid()) {
                spdlog::warn(
                    "Could not find project \"{}\" for template task \"{}\"",
                    projectSrc, name);
                continue;
            }
            project = projectObj.value<clockify::Project>().name();
            projectId = projectObj.value<clockify::Project>().id();
        }

        tasks.emplace_back(
            new TaskListItem(name, description, project, projectId));
    }
    _templateTasks->swapTasks(tasks);
}

QCoro::Task<> Clockify::updateActiveTask() {
    using namespace date::literals;

    auto entry = co_await _activeWorkspace->getCurrentTimeEntry();
    if (entry.has_value() != _activeEntry.running ||
        (entry.has_value() && entry->id() != _activeEntry.id)) {
        _activeEntry.running = entry.has_value();

        if (entry.has_value()) {
            _activeEntry.startTime = entry->start();
            _activeEntry.id = entry->id();
            _activeEntry.description =
                entry->description() ? *entry->description() : "";

            auto &project = entry->project();
            if (project) {
                _activeEntry.project = project->name();
                _activeEntry.projectColor = QColor(project->color());
            } else {
                _activeEntry.project = QStringLiteral("");
                _activeEntry.projectColor = QColorConstants::Black;
            }
        } else {
            _activeEntry.id = QStringLiteral("");
        }
        emit taskRunningChanged();
        emit activeTaskDescriptionChanged();
        emit activeTaskProjectChanged();
        emit activeTaskStartTimeChanged();
        emit activeTaskProjectColorChanged();
    }
}

QCoro::Task<> Clockify::updateTodaysEntries() {
    if (!_activeWorkspace) {
        _todaysEntries->clear();
        co_return;
    }
    auto beginningOfDay =
        QDateTime(QDateTime::currentDateTime().date(), QTime{});
    auto endOfDay = beginningOfDay.addDays(1);
    auto entries =
        co_await _activeWorkspace->getTimeEntries(beginningOfDay, endOfDay);
    entries.erase(std::remove_if(entries.begin(), entries.end(),
                                 [](const clockify::TimeEntry &e) {
                                     return !e.end().has_value();
                                 }),
                  entries.end());
    _todaysEntries->replace(entries.begin(), entries.end());
}
