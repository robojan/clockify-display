#include "settings.h"

#include <json_helpers.h>
#include <spdlog/spdlog.h>
#include <spdlog_formatters.h>

#include <QDir>
#include <QStandardPaths>
#include <nlohmann/json.hpp>

static QString jsonToString(const nlohmann::json &j, const char *key,
                            const std::string_view &def = "") {
    return QString::fromUtf8(j.value(key, def));
    // return j.is_null() ? QString::fromUtf8(def)
    //                    : QString::fromUtf8(j.get<std::string>());
}

static bool jsonToBoolean(const nlohmann::json &j, const char *key,
                          bool def = false) {
    return j.value(key, def);
    // j.is_null() ? def : j.get<bool>();
}

Settings *Settings::create(QQmlEngine *qmlEngine, QJSEngine *jsEngine) {
    Q_UNUSED(qmlEngine);
    Q_UNUSED(jsEngine);
    return &instance();
}

Settings &Settings::instance() {
    static QPointer<Settings> s_instance = new Settings;
    return *s_instance;
}

Settings::Settings() {
    auto logger = spdlog::get(kLoggingName);
    if (!logger) {
        spdlog::initialize_logger(
            spdlog::default_logger()->clone(kLoggingName));
    }
    load();
}

void Settings::load() {
    auto logger = spdlog::get(kLoggingName);
    auto path = getConfigFilePath();
    logger->info("Loading settings from file: \"{}\"", path);

    QFile file(path);
    if (!file.open(QFile::Text | QFile::ReadOnly)) {
        logger->error("Could not open settings file: {}", file.errorString());
        return;
    }

    auto raw = file.readAll();
    auto root = nlohmann::json::parse(std::string_view(raw.data(), raw.size()));

    _clockify = root["clockify"];
    _gui = root["gui"];
    _atlassian = root["atlassian"];
    _epicMappings = root["epic_mappings"];
    std::ranges::for_each(root["tasks"],
                          [&](auto &j) { _tasks.push_back(new TaskItem(j)); });
}

void Settings::save() const {
    auto logger = spdlog::get(kLoggingName);
    auto path = getConfigFilePath();
    logger->info("Saving settings to file: \"{}\"", path);

    auto root = nlohmann::json::object();

    root["clockify"] = _clockify.serialize();
    root["atlassian"] = _atlassian.serialize();
    root["gui"] = _gui.serialize();
    root["epic_mappings"] = _epicMappings.serialize();
    for (auto t : _tasks) root["tasks"].push_back(t->serialize());

    auto serialized = root.dump(4);
    QFile file(path);
    if (!file.open(QFile::Text | QFile::WriteOnly)) {
        logger->error("Could not open settings file: {}", file.errorString());
        return;
    }

    file.write(serialized.data(), serialized.size());
}

QString Settings::getConfigFilePath() {
    // First try to find a config file in the default locations, if not found
    // return a default path.

    const auto configFileName = QString::fromLocal8Bit(kConfigFileName);

    if (QDir pwdDir{QDir::currentPath()}; pwdDir.exists(configFileName)) {
        return pwdDir.filePath(configFileName);
    }

    auto path = QStandardPaths::locate(QStandardPaths::AppConfigLocation,
                                       configFileName);
    if (path.isEmpty()) {
        QDir dir{QStandardPaths::writableLocation(
            QStandardPaths::AppConfigLocation)};
        path = dir.filePath(configFileName);
    }
    return path;
}

ClockifySettings &ClockifySettings::operator=(const nlohmann::json &j) {
    if (j.is_null()) return *this;
    _apiKey = jsonToString(j, "apikey");
    return *this;
}

nlohmann::json ClockifySettings::serialize() const {
    nlohmann::json j;
    j["apikey"] = std::string_view(_apiKey.toUtf8());
    return j;
}

void ClockifySettings::setApiKey(const QString &apiKey) {
    if (apiKey != _apiKey) {
        _apiKey = apiKey;
        emit apiKeyChanged();
    }
}

GuiSettings &GuiSettings::operator=(const nlohmann::json &j) {
    if (j.is_null()) return *this;
    _fullscreen = jsonToBoolean(j, "fullscreen");
    return *this;
}

nlohmann::json GuiSettings::serialize() const {
    nlohmann::json j;
    j["fullscreen"] = _fullscreen;
    return j;
}

void GuiSettings::setFullscreen(bool fullscreen) {
    if (fullscreen != _fullscreen) {
        _fullscreen = fullscreen;
        emit fullscreenChanged();
    }
}

TaskItem::TaskItem(const nlohmann::json &j) { *this = j; }

TaskItem &TaskItem::operator=(const nlohmann::json &j) {
    if (j.is_null()) return *this;
    _name = jsonToString(j, "name");
    _description = jsonToString(j, "description");
    _project = jsonToString(j, "project");
    _task = jsonToString(j, "task");
    return *this;
}

nlohmann::json TaskItem::serialize() const {
    nlohmann::json j;
    j["name"] = _name.toUtf8();
    if (!_description.isEmpty()) j["description"] = _description.toUtf8();
    if (!_project.isEmpty()) j["project"] = _project.toUtf8();
    if (!_task.isEmpty()) j["task"] = _task.toUtf8();

    return j;
}

void TaskItem::setName(const QString &x) {
    if (x != _name) {
        _name = x;
        emit nameChanged();
    }
}

void TaskItem::setDescription(const QString &x) {
    if (x != _description) {
        _description = x;
        emit descriptionChanged();
    }
}

void TaskItem::setProject(const QString &x) {
    if (x != _project) {
        _project = x;
        emit projectChanged();
    }
}

void TaskItem::setTask(const QString &x) {
    if (x != _task) {
        _task = x;
        emit taskChanged();
    }
}

AtlassianSettings &AtlassianSettings::operator=(const nlohmann::json &j) {
    if (j.is_null()) return *this;
    _username = j.value<QString>("username", "");
    _password = j.value<QString>("password", "");
    _host = j.value<QString>("host", "jira.atlassian.com");
    _customFields.clear();
    if (j.contains("custom_fields")) {
        for (auto &item : j["custom_fields"].items()) {
            if (item.value().is_number()) {
                auto field = u"customfield_%1"_qs.arg(item.value().get<int>());
                _customFields.insert(QString::fromStdString(item.key()), field);
            } else {
                _customFields.insert(QString::fromStdString(item.key()),
                                     item.value().get<QString>());
            }
        }
    }
    return *this;
}

nlohmann::json AtlassianSettings::serialize() const {
    nlohmann::json j;
    j["username"] = _username;
    j["password"] = _password;
    j["host"] = _host;
    return j;
}

void AtlassianSettings::setUsername(const QString &username) {
    if (username != _username) {
        _username = username;
        emit usernameChanged();
    }
}

void AtlassianSettings::setPassword(const QString &password) {
    if (password != _password) {
        _password = password;
        emit passwordChanged();
    }
}

void AtlassianSettings::setHost(const QString &host) {
    if (host != _host) {
        _host = host;
        emit hostChanged();
    }
}

void AtlassianSettings::setCustomFields(
    const QHash<QString, QString> &customFields) {
    if (customFields != _customFields) {
        _customFields = customFields;
        emit customFieldsChanged();
    }
}

EpicMappingSettings &EpicMappingSettings::operator=(const nlohmann::json &j) {
    if (j.is_null()) return *this;
    for (auto &item : j.items()) {
        auto epic = QString::fromStdString(item.key());
        auto &info = item.value();
        ClockifyProjectAndTask pat;
        info["project"].get_to(pat.project);
        info["task"].get_to(pat.task);
        _mappings.insert(epic, pat);
    }
    return *this;
}

nlohmann::json EpicMappingSettings::serialize() const {
    auto j = nlohmann::json::object();
    for (auto it : _mappings.asKeyValueRange()) {
        auto epic = it.first.toStdString();
        auto info = nlohmann::json::object();
        info["project"] = it.second.project;
        info["task"] = it.second.task;
        j[epic] = info;
    }
    return j;
}

QVariant EpicMappingSettings::getMapping(const QString &epic) const {
    auto it = _mappings.find(epic);
    return it == _mappings.end() ? QVariant{} : QVariant::fromValue(*it);
}
