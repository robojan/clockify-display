#pragma once

#include <QDateTime>
#include <QString>
#include <QUrl>
#include <nlohmann/json.hpp>
#include <optional>

QT_BEGIN_NAMESPACE
inline void to_json(nlohmann::json &j, const QString &str) {
    j = nlohmann::json{str.toStdString()};
}
inline void from_json(const nlohmann::json &j, QString &str) {
    str = QString::fromStdString(j.get<std::string>());
}
inline void to_json(nlohmann::json &j, const QStringList &list) {
    j = nlohmann::json::array();
    for (auto &str : list) {
        j.push_back(str);
    }
}
inline void from_json(const nlohmann::json &j, QStringList &list) {
    list.clear();
    list.reserve(j.size());
    for (auto &e : j) {
        list.emplace_back(e.get<QString>());
    }
}
inline void to_json(nlohmann::json &j, const QDateTime &dt) {
    j = nlohmann::json(dt.toString(Qt::ISODateWithMs));
}
inline void from_json(const nlohmann::json &j, QDateTime &dt) {
    dt = QDateTime::fromString(j.get<QString>(), Qt::ISODateWithMs);
}
inline void to_json(nlohmann::json &j, const QUrl &url) {
    j = nlohmann::json(url.toString());
}
inline void from_json(const nlohmann::json &j, QUrl &url) {
    url = QUrl(j.get<QString>());
}
QT_END_NAMESPACE

namespace clockify::detail {

inline std::optional<QString> getOptionalString(const nlohmann::json &j,
                                                const char *name) {
    auto val = j[name];
    return val.is_null() ? std::nullopt
                         : std::make_optional(
                               QString::fromStdString(val.get<std::string>()));
}

inline QString getString(const nlohmann::json &j, const char *name) {
    return QString::fromStdString(j[name].get<std::string>());
}

inline std::optional<QDateTime> getOptionalTimestamp(const nlohmann::json &j,
                                                     const char *name) {
    auto dateStr = getOptionalString(j, name);
    return dateStr ? std::make_optional(
                         QDateTime::fromString(dateStr.value(), Qt::ISODate))
                   : std::nullopt;
}

inline QDateTime getTimestamp(const nlohmann::json &j, const char *name) {
    return QDateTime::fromString(getString(j, name), Qt::ISODate);
}

}  // namespace clockify::detail
