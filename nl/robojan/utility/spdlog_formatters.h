#pragma once

#include <spdlog/fmt/fmt.h>
#include <spdlog/spdlog.h>

#include <QByteArray>
#include <QString>
#include <QStringView>
#include <QUrl>

template <>
struct fmt::formatter<QString> : fmt::formatter<string_view> {
    template <typename FormatContext>
    auto format(const QString &str, FormatContext &ctx) const {
        return formatter<string_view>::format(str.toStdString(), ctx);
    }
};

template <>
struct fmt::formatter<QStringView> : fmt::formatter<string_view> {
    template <typename FormatContext>
    auto format(QStringView str, FormatContext &ctx) const {
        QByteArray u8Str = str.toUtf8();
        return formatter<string_view>::format(u8Str.toStdString(), ctx);
    }
};

template <>
struct fmt::formatter<QUrl> : fmt::formatter<QString> {
    template <typename FormatContext>
    auto format(QUrl url, FormatContext &ctx) const {
        return formatter<QString>::format(url.toDisplayString(), ctx);
    }
};

template <>
struct fmt::formatter<QByteArray> : fmt::formatter<string_view> {
    template <typename FormatContext>
    auto format(const QByteArray &arr, FormatContext &ctx) const {
        return formatter<string_view>::format(arr.toStdString(), ctx);
    }
};
