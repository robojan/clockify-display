#include "jira_interface.h"

#include <jira_error.h>
#include <spdlog/spdlog.h>
#include <spdlog_formatters.h>

#include <QCoroNetwork>
#include <QCoroNetworkReply>
#include <QCoroTask>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <sstream>

namespace jira::detail {
static const QString kJiraApiPath{"/rest/api/2/"};

QString Interface::createBaseUrl(const QString &host) {
    if (host.isEmpty()) {
        spdlog::error("Missing host specified for atlassian services");
        return QStringLiteral("");
    }
    return u"https://"_qs + host + kJiraApiPath;
}

QByteArray Interface::crateBasicAuthUrl(const QString &username,
                                        const QString &password) {
    if (username.isEmpty()) {
        spdlog::error("Missing username for atlassian services");
        return {};
    }
    if (password.isEmpty()) {
        spdlog::error("Missing password for atlassian services");
        return {};
    }
    QByteArray credentials;
    credentials.append(username.toUtf8());
    credentials.append(':');
    credentials.append(password.toUtf8());

    QByteArray auth("Basic ");
    return auth.append(credentials.toBase64());
}

Interface::Interface(const QString &host, const QString &username,
                     const QString &password)
    : _baseUrl(createBaseUrl(host)),
      _basicAuthHeader(crateBasicAuthUrl(username, password)) {}

void Interface::addCommonHeaders(QNetworkRequest &request) const {
    request.setRawHeader("Authorization", _basicAuthHeader);
    request.setRawHeader("Accept", "application/json");
    request.setHeader(QNetworkRequest::UserAgentHeader, "rj-display/0.1");
}

nlohmann::json Interface::readResponse(QNetworkReply &response,
                                       QNetworkReply::NetworkError okCode,
                                       QStringView uri) const {
    auto response_body = response.readAll();

    spdlog::trace("Response:{}\n{}", uri, QString::fromUtf8(response_body));

    if (response_body.isEmpty()) {
        spdlog::error("Got no body from {}", uri);
        throw ConnectionError();
    }

    try {
        auto response_object = nlohmann::json::parse(response_body);

        if (response.error() != okCode) {
            spdlog::error("error retrieving {}[{}]: {}", uri, response.error(),
                          QString::fromUtf8(response_body));
            throw JiraError(response.error(), response_object);
        }
        return response_object;
    } catch (nlohmann::json::exception &e) {
        spdlog::error("Error parsing reply to {}:\n{}", uri, e.what());
        throw ProtocolError();
    }
}

QCoro::Task<nlohmann::json> Interface::apiGet(
    const QString &path, const QUrlQuery &query,
    QNetworkReply::NetworkError okCode) const {
    auto uri = QUrl(_baseUrl + path);
    uri.setQuery(query);

    spdlog::trace("GET {}", uri);

    QNetworkRequest request(uri);
    addCommonHeaders(request);

    QScopedPointer<QNetworkReply> reply{co_await _nam->get(request)};

    co_return readResponse(*reply, okCode, uri.toDisplayString());
}

QCoro::Task<nlohmann::json> Interface::apiPost(
    const QString &path, const nlohmann::json &j, const QUrlQuery &query,
    QNetworkReply::NetworkError okCode) const {
    auto uri = QUrl(_baseUrl + path);
    uri.setQuery(query);

    spdlog::trace("POST {}", uri);

    QNetworkRequest request(uri);
    addCommonHeaders(request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    auto request_body = QByteArray::fromStdString(j.dump());
    QScopedPointer<QNetworkReply> reply{
        co_await _nam->post(request, request_body)};

    co_return readResponse(*reply, okCode, uri.toDisplayString());
}

QCoro::Task<nlohmann::json> Interface::apiPatch(
    const QString &path, const nlohmann::json &j, const QUrlQuery &query,
    QNetworkReply::NetworkError okCode) const {
    auto uri = QUrl(_baseUrl + path);
    uri.setQuery(query);

    spdlog::trace("PATCH {}", uri);

    QNetworkRequest request(uri);
    addCommonHeaders(request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    auto request_body = QByteArray::fromStdString(j.dump());
    QScopedPointer<QNetworkReply> reply{
        co_await _nam->sendCustomRequest(request, "PATCH", request_body)};

    co_return readResponse(*reply, okCode, uri.toDisplayString());
}

QCoro::Task<bool> Interface::init() { co_return true; }

QString Interface::getCustomField(const QString &fieldName) const {
    auto it = _customFields.find(fieldName);
    return it == _customFields.end() ? QString{} : *it;
}

}  // namespace jira::detail
