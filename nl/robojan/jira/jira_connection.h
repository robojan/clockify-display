#pragma once

#include <atlassian_global.h>

#include <QCoroTask>
#include <QString>

#include "atlassian_paged_response.h"
#include "jira_interface.h"
#include "jira_issue.h"

namespace jira {
class ATLASSIAN_EXPORT Connection {
public:
    template <typename SettingsType>
    Connection(const SettingsType& settings)
        : _itf(std::make_shared<detail::Interface>(settings)) {}
    QCoro::Task<bool> init();

    Q_INVOKABLE QCoro::Task<PagedResponse<Issue>*> search(QString jql) const;

private:
    std::shared_ptr<detail::Interface> _itf;
};

}  // namespace jira
