#include "atlassian_paged_response.h"

namespace jira {

PagedResponseBase::PagedResponseBase(std::shared_ptr<detail::Interface> itf,
                                     const QString &path,
                                     const QUrlQuery &query,
                                     const std::string &valuesKey,
                                     qsizetype blockSize, QObject *parent)
    : QAbstractListModel(parent),
      _itf(std::move(itf)),
      _path(path),
      _query(query),
      _valuesKey(valuesKey),
      _blockSize(blockSize) {}

QCoro::Task<nlohmann::json> PagedResponseBase::loadRaw(qsizetype startAt,
                                                       qsizetype amount) const {
    QUrlQuery q = _query;
    q.addQueryItem(u"startAt"_qs, QString::number(startAt));
    q.addQueryItem(u"maxResults"_qs, QString::number(amount));
    return _itf->apiGet(_path, q);
}

}  // namespace jira
