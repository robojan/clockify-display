#pragma once

#include <atlassian_global.h>

#include <QException>
#include <QHash>
#include <QString>
#include <QStringList>
#include <nlohmann/json.hpp>

namespace jira {
class ATLASSIAN_EXPORT JiraError : public QException {
public:
    JiraError(int statusCode, const nlohmann::json& j);

    [[nodiscard]] char const* what() const noexcept override;

private:
    int _status;
    QStringList _errorMessages;
    QHash<QString, QString> _errors;
    std::string _whatMsg;
};

class ATLASSIAN_EXPORT ConnectionError : public QException {
    [[nodiscard]] char const* what() const noexcept override {
        return "Failed to connect to the atlassian service.";
    };
};

class ATLASSIAN_EXPORT ProtocolError : public QException {
    [[nodiscard]] char const* what() const noexcept override {
        return "Received an invalid response from the atlassian service.";
    };
};
}  // namespace jira
