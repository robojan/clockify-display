#include "jira_issue.h"

#include <json_helpers.h>

namespace jira {

Issue::Issue(std::shared_ptr<detail::Interface> itf, const nlohmann::json &j,
             QObject *parent)
    : QObject(parent), _itf(std::move(itf)) {
    _fields = j["fields"];
    j["self"].get_to(_self);
    j["key"].get_to(_key);
    _fields["description"].get_to(_description);
    _fields["summary"].get_to(_description);
    _fields["issuetype"]["name"].get_to(_issueType);
    _fields["issuetype"]["iconUrl"].get_to(_issueTypeIcon);
    auto epicField = _itf->getCustomField("epic").toStdString();
    if (!epicField.empty() && _fields.contains(epicField) &&
        _fields[epicField].is_string()) {
        _fields[epicField].get_to(_epic);
    }
}

};  // namespace jira
