#pragma once

#include <QtCore/QtGlobal>

#if defined(ATLASSIAN_LIBRARY)
#define ATLASSIAN_EXPORT Q_DECL_EXPORT
#else
#define ATLASSIAN_EXPORT Q_DECL_IMPORT
#endif
