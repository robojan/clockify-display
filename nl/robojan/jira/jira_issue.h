#pragma once

#include <jira_interface.h>

#include <QObject>
#include <nlohmann/json.hpp>

namespace jira {
class ATLASSIAN_EXPORT Issue : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString key READ key CONSTANT)
    Q_PROPERTY(QString description READ description CONSTANT)
    Q_PROPERTY(QString summary READ summary CONSTANT)
    Q_PROPERTY(QString epic READ epic CONSTANT)
    Q_PROPERTY(QUrl issueTypeIcon READ issueTypeIcon CONSTANT)
    Q_PROPERTY(QString issueType READ issueType CONSTANT)
public:
    Issue(std::shared_ptr<detail::Interface> itf, const nlohmann::json &j,
          QObject *parent = nullptr);

    const QString &key() const { return _key; }
    const QString &description() const { return _description; }
    const QString &summary() const { return _summary; }
    const QString &self() const { return _self; }
    const QString &epic() const { return _epic; }
    const QUrl &issueTypeIcon() const { return _issueTypeIcon; }
    const QString &issueType() const { return _issueType; }

private:
    std::shared_ptr<detail::Interface> _itf;
    QString _self;
    QString _key;
    QString _description;
    QString _summary;
    QString _epic;
    QString _issueType;
    QUrl _issueTypeIcon;
    nlohmann::json _fields;
};
}  // namespace jira
