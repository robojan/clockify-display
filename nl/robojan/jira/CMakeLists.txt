qt_add_library(jira)
set_target_properties(jira PROPERTIES 
    AUTOMOC ON
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

qt_add_qml_module(jira
    URI nl.robojan.jira
    VERSION 1.0
    OUTPUT_TARGETS jira_targets
    AUTO_RESOURCE_PREFIX
)

target_link_libraries(jira PUBLIC utility Qt6::Core QCoro::Core QCoro::Network)

target_include_directories(jira PUBLIC .)

target_sources(jira PRIVATE
    jira_connection.h
    jira_connection.cpp
    jira_interface.h
    jira_interface.cpp
    jira_error.h
    jira_error.cpp
    atlassian_paged_response.cpp
    atlassian_paged_response.h
    jira_issue.cpp
    jira_issue.h
)

target_compile_definitions(jira PRIVATE ATLASSIAN_LIBRARY)

install(TARGETS jira ${jira_targets}
    EXPORT jira_targets
    FILE_SET HEADERS
    LIBRARY ARCHIVE RUNTIME
    INCLUDES DESTINATION include)
