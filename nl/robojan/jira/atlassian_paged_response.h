#pragma once

#include <atlassian_global.h>
#include <jira_interface.h>

#include <QAbstractListModel>
#include <QMap>
#include <QObject>
#include <QQmlEngine>
#include <QString>
#include <QUrlQuery>
#include <memory>

namespace jira {
class ATLASSIAN_EXPORT PagedResponseBase : public QAbstractListModel {
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("Paged responses should be retrieved from API calls")

    Q_PROPERTY(QString path READ path CONSTANT)
    Q_PROPERTY(QUrlQuery query READ query CONSTANT)
    Q_PROPERTY(qsizetype count READ count NOTIFY countChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)
public:
    PagedResponseBase(std::shared_ptr<detail::Interface> itf,
                      const QString &path, const QUrlQuery &query = QUrlQuery{},
                      const std::string &valuesKey = "values",
                      qsizetype _blockSize = 50, QObject *parent = nullptr);

    virtual QVariant get(qsizetype idx) const = 0;
    QUrlQuery query() const noexcept { return _query; }
    QString path() const noexcept { return _path; }
    qsizetype count() const noexcept { return _total; }
    virtual qsizetype loaded() const noexcept = 0;
    bool loading() const noexcept { return _loading; }

    int rowCount(const QModelIndex &parent) const override {
        return parent.isValid() ? 0 : static_cast<int>(loaded());
    }

    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override {
        if (!index.isValid() && index.row() < 0 && index.row() >= count()) {
            return QVariant{};
        }
        if (role == Qt::UserRole) {
            return get(index.row());
        } else {
            return QVariant{};
        }
    }

    QHash<int, QByteArray> roleNames() const override {
        return {{Qt::UserRole, "item"}};
    }

    bool canFetchMore(const QModelIndex &parent) const override {
        return parent.isValid() ? false : loaded() < count();
    }

    void fetchMore(const QModelIndex &parent) override = 0;

signals:
    void countChanged();
    void loadingChanged();

protected:
    std::shared_ptr<detail::Interface> _itf;
    qsizetype _blockSize;
    std::string _valuesKey;

    QCoro::Task<nlohmann::json> loadRaw(qsizetype startAt,
                                        qsizetype amount) const;

    void setTotal(qsizetype total) {
        if (total != _total) {
            _total = total;
            emit countChanged();
        }
    }
    void setLoading(bool loading) {
        if (loading != _loading) {
            _loading = loading;
            emit loadingChanged();
        }
    }

private:
    QString _path;
    QUrlQuery _query;
    qsizetype _total{0};
    bool _loading{false};
};

template <typename T>
concept JiraObject =
    requires(std::shared_ptr<detail::Interface> itf, const nlohmann::json &j,
             QObject *parent) { new T(itf, j, parent); };

template <JiraObject T>
class PagedResponse : public PagedResponseBase {
public:
    PagedResponse(std::shared_ptr<detail::Interface> itf, const QString &path,
                  const QUrlQuery &query = QUrlQuery{},
                  const std::string &valuesKey = "values",
                  qsizetype blockSize = 50, QObject *parent = nullptr)
        : PagedResponseBase(itf, path, query, valuesKey, blockSize, parent) {}

    qsizetype loaded() const noexcept override { return _values.size(); }

    QCoro::Task<> load(qsizetype startAt = 0) {
        return load(startAt, _blockSize);
    }
    QCoro::Task<> load(qsizetype startAt, qsizetype amount);

    QVariant get(qsizetype idx) const override;
    void fetchMore(const QModelIndex &parent) override;

private:
    QList<T *> _values;
};

template <JiraObject T>
QCoro::Task<> PagedResponse<T>::load(qsizetype startAt, qsizetype amount) {
    setLoading(true);
    auto j = co_await loadRaw(startAt, amount);

    setTotal(j["total"].template get<qsizetype>());
    auto actualStartAt = j["startAt"].template get<qsizetype>();
    if (startAt != actualStartAt) {
        qWarning() << "Received a paged response with a different start index "
                      "than the requested index";
        setLoading(false);
        co_return;
    }

    auto &values = j[_valuesKey];
    auto lastValueToCreate = actualStartAt + values.size() - 1;
    if (actualStartAt < _values.size()) {
        auto lastValueToUpdate = lastValueToCreate < _values.size()
                                     ? lastValueToCreate
                                     : _values.size() - 1;
        // qDebug() << "Updating existing values " << actualStartAt << " - "
        //          << lastValueToUpdate;
        for (int i = actualStartAt; i <= lastValueToUpdate; i++) {
            _values[i]->deleteLater();
            _values[i] = new T(_itf, values[i - actualStartAt], this);
        }
        emit dataChanged(index(actualStartAt), index(lastValueToUpdate),
                         {Qt::UserRole});
    }
    if (lastValueToCreate >= _values.size()) {
        // qDebug() << "Inserting new values " << _values.size() << " - "
        //          << lastValueToCreate;
        beginInsertRows(QModelIndex(), _values.size(), lastValueToCreate);
        for (int i = _values.size(); i <= lastValueToCreate; i++) {
            _values.push_back(new T(_itf, values[i - actualStartAt], this));
        }
        endInsertRows();
    }
    setLoading(false);
}

template <JiraObject T>
QVariant PagedResponse<T>::get(qsizetype idx) const {
    if (idx < _values.size()) {
        return QVariant::fromValue(_values[idx]);
    } else {
        return {};
    }
}

template <JiraObject T>
void PagedResponse<T>::fetchMore(const QModelIndex &parent) {
    // If we have a valid parent then we are in one of the child items. We have
    // just a list of items so nothing to load.
    if (parent.isValid()) {
        return;
    }

    load(loaded());
}

}  // namespace jira
