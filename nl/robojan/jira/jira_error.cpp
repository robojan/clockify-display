#include "jira_error.h"

#include <json_helpers.h>

#include <sstream>

namespace jira {
JiraError::JiraError(int statusCode, const nlohmann::json &j)
    : _status(statusCode) {
    // Parse the JSON
    j["errorMessages"].get_to(_errorMessages);
    for (auto &el : j["errors"].items()) {
        _errors.insert(QString::fromStdString(el.key()),
                       el.value().get<QString>());
    }

    // Set the error message
    std::stringstream ss;
    ss << "JiraError(" << _status << "):" << std::endl;
    for (auto &msg : _errorMessages) {
        ss << "\t" << msg.toStdString() << std::endl;
    }
    for (auto it = _errors.keyValueBegin(); it != _errors.keyValueEnd(); ++it) {
        ss << "\t" << it->first.toStdString() << "=" << it->second.toStdString()
           << std::endl;
    }
    _whatMsg = ss.str();
}

const char *JiraError::what() const noexcept { return _whatMsg.c_str(); }

}  // namespace jira
