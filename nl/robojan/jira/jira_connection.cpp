#include "jira_connection.h"

#include "json_helpers.h"

namespace jira {

QCoro::Task<bool> Connection::init() { return _itf->init(); }

QCoro::Task<PagedResponse<Issue> *> Connection::search(QString jql) const {
    QUrlQuery query;
    query.addQueryItem(QStringLiteral("jql"), jql);
    auto response = new PagedResponse<Issue>(_itf, QStringLiteral("search"),
                                             query, "issues");
    co_await response->load();
    co_return response;
}

}  // namespace jira
