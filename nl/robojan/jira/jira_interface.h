#pragma once

#include <atlassian_global.h>

#include <QCoroTask>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QStringView>
#include <QUrlQuery>
#include <nlohmann/json.hpp>

namespace jira {
namespace detail {

class ATLASSIAN_EXPORT Interface : public QObject {
    Q_OBJECT
public:
    template <typename SettingsType>
    Interface(const SettingsType &settings)
        : Interface(settings.host(), settings.username(), settings.password()) {
        _customFields = settings.customFields();
    }

    Interface(const QString &host, const QString &username,
              const QString &password);
    QCoro::Task<bool> init();

    QCoro::Task<nlohmann::json> apiGet(
        const QString &path, const QUrlQuery &query = QUrlQuery{},
        QNetworkReply::NetworkError okCode = QNetworkReply::NoError) const;
    QCoro::Task<nlohmann::json> apiPost(
        const QString &path, const nlohmann::json &j,
        const QUrlQuery &query = QUrlQuery{},
        QNetworkReply::NetworkError okCode = QNetworkReply::NoError) const;
    QCoro::Task<nlohmann::json> apiPatch(
        const QString &path, const nlohmann::json &j,
        const QUrlQuery &query = QUrlQuery{},
        QNetworkReply::NetworkError okCode = QNetworkReply::NoError) const;

    QString getCustomField(const QString &fieldName) const;

protected:
    void addCommonHeaders(QNetworkRequest &request) const;

    nlohmann::json readResponse(
        QNetworkReply &response, QNetworkReply::NetworkError okCode,
        QStringView uri = QStringLiteral("<unknown>")) const;

private:
    const QString _baseUrl;
    QByteArray _basicAuthHeader;
    QNetworkAccessManager *_nam = new QNetworkAccessManager(this);
    QHash<QString, QString> _customFields;

    static QString createBaseUrl(const QString &host);
    static QByteArray crateBasicAuthUrl(const QString &username,
                                        const QString &password);
};
}  // namespace detail
}  // namespace jira
