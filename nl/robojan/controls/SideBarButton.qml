import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

Rectangle {
    property alias checked: button.checked
    property alias iconSource: button.icon.source
    
    signal clicked()
    
    id: root

    color: button.checked ? "#40ffffff" : "transparent"

    ToolButton {
        id: button
        anchors.fill: parent

        icon.color: checked ? "transparent" : "white"        

        onClicked: root.clicked()
    }
}
