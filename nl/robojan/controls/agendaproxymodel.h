#pragma once

#include <QIdentityProxyModel>
#include <QObject>
#include <QQmlEngine>
#include <QString>

class AgendaProxyModel : public QIdentityProxyModel {
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QString startRole WRITE setStartRole READ getStartRole NOTIFY
                   startRoleChanged)
    Q_PROPERTY(
        QString endRole WRITE setEndRole READ getEndRole NOTIFY endRoleChanged)
public:
    explicit AgendaProxyModel(QObject *parent = nullptr);

    const QString &getStartRole() const { return _startRole; }
    void setStartRole(const QString &role);
    const QString &getEndRole() const { return _endRole; }
    void setEndRole(const QString &role);

    bool isValid() const;

    QVariant data(const QModelIndex &proxyIndex,
                  int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

signals:
    void startRoleChanged();
    void endRoleChanged();

private:
    struct MetaData {
        int column = 0;
        int numColumns = 0;
    };

    QString _startRole;
    int _startRoleIdx = -1;
    QString _endRole;
    int _endRoleIdx = -1;
    int _decimalStartRoleIdx = -1;
    int _decimalEndRoleIdx = -1;
    int _numColumnsRoleIdx = -1;
    int _columnRoleIdx = -1;

    // This is a vector that contains the indices of the
    std::vector<MetaData> _metaData;

    static float timeToDecimalTime(const QTime &t);

private slots:
    void updateRoleIndices();
    void updateMetaData();
};
