import QtQuick
import QtQuick.Shapes
import QtQuick.Controls
import nl.robojan.controls

Rectangle {
    property double hourHeight: 30
    property double startTime: 8
    property bool hasHeader: true
    // property var background: 
    property var model: []
    property string startRole: "start"
    property string endRole: "end"

    clip: true
    id: control
    
    border.color: control.Material.frameColor 
    border.width: 1
    color: "transparent"

    function timeToY(time) {
        return time * hourHeight;
    }

    Item {
        id: constants
        
        TextMetrics {
            id: timeTextMetrics
            text: "88:88" // The widest time possible
        }

        readonly property double desiredHeaderWidth: timeTextMetrics.width + 10
        readonly property double minimumContentWidth: 50
        readonly property bool actuallyHasHeader: (control.hasHeader && control.width >= desiredHeaderWidth + minimumContentWidth)
        readonly property double headerWidth: actuallyHasHeader ? desiredHeaderWidth : 0
        readonly property double dayWidth: actuallyHasHeader ? control.width - headerWidth : control.width
        readonly property double dayOffset: actuallyHasHeader ? desiredHeaderWidth : 0
    }

    AgendaProxyModel {
        id: proxyModel
        sourceModel: control.model
        startRole: control.startRole
        endRole: control.endRole
    }


    Flickable {
        anchors.fill: control
        flickableDirection: Flickable.VerticalFlick
        contentWidth: contents.childrenRect.width
        contentHeight: contents.childrenRect.height
        contentY: timeToY(startTime)
        Item {
            id: contents
            width: childrenRect.width
            height: childrenRect.height

            Rectangle {
                id: headerBorderLine
                x: constants.dayOffset - 1
                y: 0
                height: 24 * control.hourHeight
                width: 1
                color: control.Material.rippleColor
            }

            Repeater {
                id: background
                model: 24
                delegate: Item {
                    id: hourBackground
                    x: 0
                    y: control.timeToY(index)
                    width: control.width
                    height: control.hourHeight
                    // color: control.Material.frameColor 

                    Item {
                        id: hourHeader
                        width: constants.headerWidth
                        height: parent.height
                        Text {
                            text: `${index}:00`
                            visible: constants.actuallyHasHeader
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.top: parent.top
                            horizontalAlignment: Text.AlignHCenter
                            color: control.Material.secondaryTextColor
                        }
                    }

                    Rectangle {
                        x: 0
                        y: parent.height - 1
                        height: 1
                        width: parent.width
                        color: control.Material.rippleColor
                    }
                }
            }

            Repeater {
                id: items
                model: proxyModel
                delegate: Rectangle {
                    readonly property double columnWidth: constants.dayWidth / numColumns
                    readonly property double targetHeight: timeToY(decimalEndTime) - y
                    x: column * columnWidth + constants.dayOffset
                    y: timeToY(decimalStartTime)
                    width: columnWidth
                    height: targetHeight < 1 ? 5 : targetHeight
                    color: control.Material.accentColor
                    clip: true
                    radius: 3

                    Text {
                        text: description
                        color: control.Material.primaryTextColor
                    }
                }
            }
        }

    }

}
