#include "agendaproxymodel.h"

#include <QDateTime>
#include <algorithm>
#include <ranges>

AgendaProxyModel::AgendaProxyModel(QObject *parent)
    : QIdentityProxyModel(parent) {
    connect(this, &QIdentityProxyModel::sourceModelChanged, this,
            &AgendaProxyModel::updateRoleIndices);
    connect(this, &QIdentityProxyModel::dataChanged, this,
            &AgendaProxyModel::updateMetaData);
    connect(this, &QIdentityProxyModel::rowsInserted, this,
            &AgendaProxyModel::updateMetaData);
    connect(this, &QIdentityProxyModel::rowsRemoved, this,
            &AgendaProxyModel::updateMetaData);
    connect(this, &QIdentityProxyModel::modelReset, this,
            &AgendaProxyModel::updateMetaData);
    connect(this, &QIdentityProxyModel::modelReset, this,
            &AgendaProxyModel::updateRoleIndices);
}

void AgendaProxyModel::setStartRole(const QString &role) {
    if (role != _startRole) {
        _startRole = role;
        updateRoleIndices();
        emit startRoleChanged();
    }
}

void AgendaProxyModel::setEndRole(const QString &role) {
    if (role != _endRole) {
        _endRole = role;
        updateRoleIndices();
        emit endRoleChanged();
    }
}

void AgendaProxyModel::updateRoleIndices() {
    if (sourceModel()) {
        auto roles = sourceModel()->roleNames();
        _startRoleIdx = roles.key(_startRole.toUtf8(), -1);
        _endRoleIdx = roles.key(_endRole.toUtf8(), -1);
        if (_startRoleIdx == -1) {
            qWarning() << "Could not find the role " << _startRole
                       << " in the model";
        }
        if (_endRoleIdx == -1) {
            qWarning() << "Could not find the role " << _endRole
                       << " in the model";
        }

        // Find the last used key
        auto roleKeys = roles.keys();
        int lastRole = *std::ranges::max_element(roleKeys);

        _decimalStartRoleIdx = lastRole + 1;
        _decimalEndRoleIdx = lastRole + 2;
        _numColumnsRoleIdx = lastRole + 3;
        _columnRoleIdx = lastRole + 4;
        updateMetaData();
    } else {
        _endRoleIdx = -1;
        _startRoleIdx = -1;
        _decimalStartRoleIdx = -1;
        _decimalEndRoleIdx = -1;
        _numColumnsRoleIdx = -1;
        _columnRoleIdx = -1;
    }
}

bool AgendaProxyModel::isValid() const {
    return _endRoleIdx >= 0 && _startRoleIdx >= 0 &&
           _decimalStartRoleIdx >= 0 && _decimalEndRoleIdx >= 0 &&
           _numColumnsRoleIdx >= 0 && _columnRoleIdx >= 0;
}

QVariant AgendaProxyModel::data(const QModelIndex &proxyIndex, int role) const {
    if (!proxyIndex.isValid() || proxyIndex.row() < 0 ||
        proxyIndex.row() >= rowCount()) {
        return QVariant{};
    }

    if (role == _decimalStartRoleIdx) {
        return timeToDecimalTime(
            QIdentityProxyModel::data(proxyIndex, _startRoleIdx)
                .toDateTime()
                .toLocalTime()
                .time());
    } else if (role == _decimalEndRoleIdx) {
        return timeToDecimalTime(
            QIdentityProxyModel::data(proxyIndex, _endRoleIdx)
                .toDateTime()
                .toLocalTime()
                .time());
    } else if (role == _numColumnsRoleIdx) {
        return _metaData[proxyIndex.row()].numColumns;
    } else if (role == _columnRoleIdx) {
        return _metaData[proxyIndex.row()].column;
    } else {
        return QIdentityProxyModel::data(proxyIndex, role);
    }
}

QHash<int, QByteArray> AgendaProxyModel::roleNames() const {
    if (sourceModel()) {
        auto result = sourceModel()->roleNames();
        if (isValid()) {
            result.insert(_decimalStartRoleIdx, "decimalStartTime");
            result.insert(_decimalEndRoleIdx, "decimalEndTime");
            result.insert(_numColumnsRoleIdx, "numColumns");
            result.insert(_columnRoleIdx, "column");
        }
        return result;
    } else {
        return {};
    }
}

float AgendaProxyModel::timeToDecimalTime(const QTime &t) {
    return static_cast<float>(t.hour()) +
           static_cast<float>(t.minute()) / 60.0f +
           static_cast<float>(t.second()) / (60.0f * 60.0f);
}

void AgendaProxyModel::updateMetaData() {
    // Calculate the MetaData from the souce entries

    // If we don't have a source model, or not all roles are known, then we
    // don't have metadata.
    if (!sourceModel() || !isValid()) {
        _metaData.clear();
        return;
    }

    // Keep track of the assigned entries in a vector. This vector contains a
    // vector of entries per column. The number of columns can be determined at
    // the end by looking at the size of the vector.
    struct Entry {
        int idx;
        float startTime;
        float endTime;
    };
    std::vector<std::vector<Entry>> columns;
    auto hasOverlap = [](const std::vector<Entry> &column, const Entry &e1) {
        return std::ranges::find_if(column, [&e1](const Entry &e2) {
                   return !(e1.endTime <= e2.startTime ||
                            e1.startTime >= e2.endTime);
               }) != column.end();
    };

    // Insert the elements
    auto srcMdl = sourceModel();
    for (int rowIdx = 0; rowIdx < srcMdl->rowCount(); rowIdx++) {
        auto srcStartDateTime = srcMdl->index(rowIdx, 0).data(_startRoleIdx);
        auto srcEndDateTime = srcMdl->index(rowIdx, 0).data(_endRoleIdx);
        if (!srcStartDateTime.canConvert<QDateTime>() ||
            !srcEndDateTime.canConvert<QDateTime>()) {
            qWarning() << "Cannot retrieve start or end datetime from the "
                          "model.";
            return;
        }

        auto decimalStartTime =
            timeToDecimalTime(srcStartDateTime.toDateTime().time());
        auto decimalEndTime =
            timeToDecimalTime(srcEndDateTime.toDateTime().time());

        // Find a column which we can freely insert this item into
        bool foundColumn = false;
        Entry entry{rowIdx, decimalStartTime, decimalEndTime};
        for (auto &column : columns) {
            if (!hasOverlap(column, entry)) {
                column.push_back(entry);
                foundColumn = true;
                break;
            }
        }
        // If we did not find a column, add a column
        if (!foundColumn) {
            columns.emplace_back().push_back(entry);
        }
    }

    // Now create the metadata structure from the assignments.
    _metaData.clear();
    _metaData.resize(srcMdl->rowCount());
    for (int columnIdx = 0; columnIdx < columns.size(); columnIdx++) {
        auto &column = columns[columnIdx];
        for (auto &entry : column) {
            _metaData[entry.idx].column = columnIdx;
            int numColumns = columnIdx + 1;
            for (int overlapColumnIdx = numColumns;
                 overlapColumnIdx < columns.size(); overlapColumnIdx++) {
                if (hasOverlap(columns[overlapColumnIdx], entry)) {
                    numColumns = overlapColumnIdx + 1;
                }
            }
            _metaData[entry.idx].numColumns = numColumns;
        }
    }
}
