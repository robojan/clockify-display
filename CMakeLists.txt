cmake_minimum_required(VERSION 3.21)

# Experimental feature of vcpkg to install dependency dlls, just like it copies dlls on build
set(X_VCPKG_APPLOCAL_DEPS_INSTALL ON CACHE BOOL "Install dependencies")

if(DEFINED ENV{VCPKG_ROOT} AND NOT DEFINED CMAKE_TOOLCHAIN_FILE)
      set(CMAKE_TOOLCHAIN_FILE "$ENV{VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake"
        CACHE STRING "")
endif()

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

project(clockify_display CXX)

set(QT_QML_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/qml)
set(QML_IMPORT_PATH ${CMAKE_SOURCE_DIR} ${CMAKE_SOURCE_DIR}/qml ${CMAKE_BINARY_DIR}/qml CACHE STRING "" FORCE)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(spdlog REQUIRED)
find_package(Qt6 6.5 COMPONENTS Core Gui Quick Network WebEngineQuick REQUIRED)
find_package(fmt REQUIRED)
find_package(nlohmann_json CONFIG REQUIRED)
find_package(date CONFIG REQUIRED)
find_package(QCoro6 REQUIRED COMPONENTS Core Network Qml)
find_package(Sanitizers)

# Set the Qt policies
qt_policy(SET QTP0001 NEW) # use the default import path of :/qt/qml

qt_add_executable(clockify WIN32)
set_property(TARGET clockify PROPERTY AUTOMOC ON)

qt_add_qml_module(clockify 
    URI nl.robojan.display 
    VERSION 1.0)

add_subdirectory(nl/robojan)
add_subdirectory(src)
add_subdirectory(qml)
add_subdirectory(resources)

target_link_libraries(clockify PRIVATE
    qml_controlsplugin utility jira
    spdlog::spdlog nlohmann_json::nlohmann_json fmt::fmt
    QCoro::Core QCoro::Network QCoro::Qml
    Qt::Core Qt::Gui Qt::Quick Qt::WebEngineQuick
    )


target_include_directories(clockify PRIVATE src)


set_target_properties(clockify PROPERTIES
    WIN32_EXECUTABLE TRUE
    MACOSX_BUNDLE TRUE
    INSTALL_RPATH "$ORIGIN/../lib"
)

add_sanitizers(clockify)
add_sanitizers(qml_controls)
add_sanitizers(qml_controlsplugin)
add_sanitizers(jira)
add_sanitizers(jiraplugin)

install(TARGETS clockify
    # RUNTIME_DEPENDENCIES
    RUNTIME DESTINATION "bin"
    BUNDLE DESTINATION "."
    LIBRARY DESTINATION "lib"
    BUNDLE
        DESTINATION .
)
install(IMPORTED_RUNTIME_ARTIFACTS clockify
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    BUNDLE DESTINATION .)

if(UNIX AND CMAKE_CROSSCOMPILING AND NOT ANDROID AND NOT APPLE AND QT6_IS_SHARED_LIBS_BUILD)
    message(STATUS "Workaround for deploying to cross compiled linux")
    
    qt6_generate_deploy_script(
            TARGET clockify
            NAME qml_app_clockify
            OUTPUT_SCRIPT deploy_script
            CONTENT "
qt_deploy_qml_imports(TARGET clockify PLUGINS_FOUND plugins_found)
qt_deploy_runtime_dependencies(
    EXECUTABLE $<TARGET_FILE:clockify>
    ADDITIONAL_MODULES \${plugins_found}
    GENERATE_QT_CONF)")

    file(GENERATE OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/qt_override_deploytool.cmake" CONTENT
"set(__QT_DEPLOY_TOOL \"GRD\")
")
    _qt_internal_add_deploy_support("${CMAKE_CURRENT_BINARY_DIR}/qt_override_deploytool.cmake")

    # Patch the deploy support to enforce the use of the generic deploy tool
    # file(READ ${QT_DEPLOY_SUPPORT} deploy_support_contents)
    # string(REPLACE "__QT_DEPLOY_TOOL \"\"" "__QT_DEPLOY_TOOL \"GRD\"" deploy_support_contents "${deploy_support_contents}")
    # file(WRITE ${QT_DEPLOY_SUPPORT} "${deploy_support_contents}")
else()
    # For windows and linux native generate the windows deploy tool script
    qt_generate_deploy_qml_app_script(
        TARGET clockify 
        OUTPUT_SCRIPT deploy_script 
        NO_UNSUPPORTED_PLATFORM_ERROR)
endif()
install(SCRIPT ${deploy_script})    # Add its runtime dependencies

include(InstallRequiredSystemLibraries)
