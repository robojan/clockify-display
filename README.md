# native dependencies
Some vcpkg packages need native dependencies install them on ubuntu with:
```
sudo apt install libxkbcommon-dev libxrender-dev libx11-dev libxshmfence-dev libtool libtool-bin autoconf autoconf-archive flex bison \
    libxext-dev libxfixes-dev libxi-dev libxcb1-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-image0-dev libxcb-shm0-dev \
    libxcb-icccm4-dev libxcb-sync-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-randr0-dev  libxcb-render-util0-dev libxcb-util-dev \
    libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev libx11-xcb-dev libgl-dev
```