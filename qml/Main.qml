import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts
import QtQuick.Window
import nl.robojan.display
import nl.robojan.controls
import QtWebEngine

ApplicationWindow {
    id: mainWindow
    objectName: "mainWindow"
    visible: true
    flags: (Settings.gui.fullscreen ? Qt.FramelessWindowHint : 0) | Qt.Window
    visibility: Settings.gui.fullscreen ? Window.FullScreen : Window.Windowed 
    title: qsTr("RJ Display")
    width: 800
    height: 480

    Material.theme: Material.Dark
    Material.primary: "#e31f40"
    Material.accent: "#3eb299"
    Material.background: "#1d252f"
    Material.foreground: "#ffffff"

    WebEngineProfile {
        id: webProfile
        storageName: "default"
        offTheRecord: false
    }

    Pane {
        id: menuBar
        width: 48
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        padding: 0

        Material.elevation: 12

        ColumnLayout {
            id: menuBarLayout
            anchors.fill: parent

            spacing: 0

            Repeater {
                model: [
                    "images/clockify-icon.svg",
                    "https://app.clockify.me/assets/favicons/favicon2.ico",
                    "https://avular.atlassian.net/s/6i46lu/b/8/e23875893aac91709fbf131183545cfe/_/favicon-software.ico",
                    "https://avular.atlassian.net/wiki/s/-230123539/6452/1b98efe95d52b22d37e916bf9052978ec7280f15/1/_/favicon-update.ico"
                ]
                SideBarButton {
                    Layout.alignment: Qt.AlignTop
                    Layout.maximumHeight: menuBar.width
                    Layout.preferredHeight: menuBar.width
                    Layout.maximumWidth: menuBar.width
                    Layout.preferredWidth: menuBar.width
                    
                    onClicked: mainStackLayout.currentIndex = index
                    checked: mainStackLayout.currentIndex == index
                    iconSource: modelData
                }
            }
            Item {
                Layout.fillHeight: true
            }
            SideBarButton {
                Layout.alignment: Qt.AlignBottom
                Layout.maximumHeight: menuBar.width
                Layout.preferredHeight: menuBar.width
                Layout.maximumWidth: menuBar.width
                Layout.preferredWidth: menuBar.width
                
                onClicked: mainStackLayout.currentIndex = mainStackLayout.count - 1
                checked: mainStackLayout.currentIndex == mainStackLayout.count - 1
                iconSource: "images/gear.svg"
            }
        }
    }
    
    StackLayout {
        id: mainStackLayout
        anchors.left: menuBar.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right

        ClockifyPage {
        }

        WebEngineView {
            url: "https://app.clockify.me/tracker"
            profile: webProfile
        }

        WebEngineView {
            url: "https://avular.atlassian.net/jira/software/c/projects/ESSENTIALS/boards/31"
            profile: webProfile
        }

        WebEngineView {
            url: "https://avular.atlassian.net/wiki/plugins/inlinetasks/mytasks.action"
            profile: webProfile
        }
        

        // Page {
        //     Text {
        //         text: "Placeholder"
        //         color: "white"
        //         anchors.fill: parent

        //         horizontalAlignment: Text.AlignHCenter
        //         verticalAlignment: Text.AlignVCenter
        //     }
        // }

        SettingsPage {
            anchors.fill: parent
        }
    }
}
