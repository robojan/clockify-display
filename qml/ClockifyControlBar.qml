
import QtQuick
import QtQuick.Controls
import nl.robojan.display 1.0
import nl.robojan.controls 1.0

Rectangle {
    id: controlBar
    implicitWidth: childrenRect.width
    color: "#ffffff"
    border.color: "#c6d2d9"
    border.width: 1

    height: 48

    StopButton {
        id: stopButton
        enabled: Clockify.taskRunning
        onClicked: Clockify.stopActiveTask();
        anchors.right: controlBar.right
        anchors.rightMargin: 10
    }

    Text {
        id: activeTaskTimer
        text: Clockify.taskRunning ? formatTime(clockifyPage.currentTime -  Clockify.activeTaskStartTime) : "00:00:00"
        height: parent.height
        anchors.right: stopButton.left
        verticalAlignment: Qt.AlignVCenter
        font.pointSize: 12
        padding: 15
        width: 100

        function formatTime(delta) {
            let ms = delta % 1000;
            delta = Math.floor(delta / 1000);
            let s = String(delta % 60).padStart(2, '0');
            delta = Math.floor(delta / 60);
            let m = String(delta % 60).padStart(2, '0');
            delta = Math.floor(delta / 60);
            let h = String(delta % 24).padStart(2, '0');
            delta = Math.floor(delta / 24);
            let d = delta;
            if(d != 0) {
                return `${d} ${h}:${m}:${s}`
            } else {
                return `${h}:${m}:${s}`
            } 
        }
    }

    Text {
        id: activeTaskProject
        enabled: Clockify.taskRunning
        text: Clockify.activeTaskProject
        color: Clockify.activeTaskProjectColor
        height: parent.height
        anchors.right: activeTaskTimer.left
        verticalAlignment: Qt.AlignVCenter
        font.pointSize: 12
        elide: Text.ElideRight
        padding: 15
        width: 150
    }

    Text {
        id: activeTaskDescription
        enabled: Clockify.taskRunning
        text: Clockify.activeTaskDescription
        elide: Text.ElideRight
        height: parent.height
        anchors.right: activeTaskProject.left
        anchors.left: controlBar.left
        verticalAlignment: Qt.AlignVCenter
        font.pointSize: 12
        padding: 15
    }
}
