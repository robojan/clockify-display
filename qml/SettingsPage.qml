import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import nl.robojan.display 1.0
import nl.robojan.controls 1.0

Page {
    id: page

    GridLayout {
        id: grid
        anchors.fill: page
        anchors.margins: 10

        TaskButton {
            text: qsTr("Quit")
            onClicked: {
                Qt.quit()
            }
        }
    }
}
