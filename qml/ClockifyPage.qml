import QtQuick
import QtQuick.Controls
import nl.robojan.display 1.0
import nl.robojan.controls 1.0

Item {
    id: clockifyPage

    property var currentTime: new Date()

    Timer {
        interval: 100
        running: true
        repeat: true
        onTriggered: {
            clockifyPage.currentTime = new Date()
        }
    }

    Timer {
        interval: 15*60*1000 // 15 minutes
        running: true
        repeat: true
        onTriggered: {
            Clockify.updateTodaysEntries()
        }
    }

    ClockifyControlBar {
        id: controlBar
        anchors.top: clockifyPage.top
        anchors.left: clockifyPage.left
        anchors.right: clockifyPage.right
        anchors.margins: 15
    }

    Item {
        anchors.top: controlBar.bottom
        anchors.left: clockifyPage.left
        anchors.right: clockifyPage.right
        anchors.bottom: clockifyPage.bottom
        anchors.margins: 15
        // color: "#b3b7bb" 
        // border.color: "#c6d2d9"
        // border.width: 1

        ClockifyTaskList {
            id: taskGrid

            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: dayView.left
            anchors.margins: 10
        }

        AgendaDayView {
            id: dayView

            width: 150
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            hourHeight: 40

            model: Clockify.todaysEntries
        }
    }

}
