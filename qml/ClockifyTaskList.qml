import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import nl.robojan.display
import nl.robojan.controls

Flickable {
    id: root

    clip: true
    contentHeight: layout.height
    contentWidth: layout.width
    ColumnLayout {
        id: layout
        spacing: 5

        Text {
            text: qsTr("Predefined")
            color: root.Material.secondaryTextColor
        }
        GridView {
            id: predefinedTasksGrid
            model: Clockify.templateTasks
            Layout.preferredWidth: root.width
            Layout.maximumHeight: Math.min(220, root.height - 50)
            Layout.preferredHeight: contentHeight
            clip: true

            cellWidth: 110
            cellHeight: 110
            delegate: TaskButton {
                text: model.name
                onClicked: Clockify.taskActivated(model.description, model.projectId, model.taskId)
            }
        }
        Item {
            implicitWidth: childrenRect.width
            implicitHeight: childrenRect.height
            Text {
                id: jiraHeaderText
                height: 20

                text: qsTr("Active jira tasks")
                color: root.Material.secondaryTextColor
                verticalAlignment: Text.AlignVCenter
            }
            BusyIndicator {
                height: jiraHeaderText.height
                width: height
                anchors.left: jiraHeaderText.right

                running: jiraTasksGrid.model?.loading ?? false
            }
        }
        GridView {
            id: jiraTasksGrid
            Layout.preferredWidth: root.width
            Layout.maximumHeight: Math.min(330, root.height - 50)
            Layout.preferredHeight: contentHeight
            clip: true

            Component.onCompleted: {
                Jira.getCurrentIssues().then((issues) => {
                    jiraTasksGrid.model = issues;
                });
            }

            cellWidth: 110
            cellHeight: 110
            delegate: TaskButton {
                text: item.description
                onClicked: {
                    if(item.epic) {
                        let info = Settings.epicMappings.getMapping(item.epic)
                        let project = Clockify.findProject(info.project)
                        Clockify.findOrCreateTask(project, info.task).then((task) => {
                            Clockify.taskActivated(item.description, project.projectId, task.taskId)
                        })
                    } else {
                        Clockify.taskActivated(item.description)
                    }
                }
                Image {
                    x: 4
                    y: 9
                    width: 16
                    height: 16
                    source: item.issueTypeIcon
                }
            }
        }
    }

}
